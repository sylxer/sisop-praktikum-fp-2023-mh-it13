# sisop-praktikum-fp-2023-MH-IT13



## Anggota
1. Sylvia Febrianti - 5027221019
2. Zulfa Hafizh Kusuma - 5027221038
3. Muhammad Rifqi Oktaviansyah - 5027221067

## database.c
```
#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <ctype.h>
#include <pthread.h>
#include <dirent.h>
#include <time.h>
#define PORT 8080

pthread_t tid[3000];

typedef struct user_t
{
    char username[30];
    char password[30];
    bool isRoot;
} User;
```
Array tid dengan 3000 elemen bertipe pthread_t digunakan untuk menyimpan identifikasi atau informasi yang berkaitan dengan thread-thread yang akan dibuat atau sedang berjalan dalam program. Sementara itu, struktur user_t merupakan blueprint yang menyimpan informasi tentang pengguna seperti nama pengguna, kata sandi, dan status apakah pengguna merupakan root atau tidak. Ini memungkinkan program untuk menyimpan dan mengelola informasi terkait pengguna dalam sistem atau aplikasi yang sedang dibangun.

```
//-------Database and Tables checker-------//

// Check if database exist
bool dbExist(char name[])
{
    char buffer[256];
    sprintf(buffer, "%s/%s", "databases", name);
    DIR *dir = opendir(buffer);
    if (dir)
    {
        closedir(dir);
        return true;
    }
    return false;
}
```
Fungsi dbExist digunakan untuk memeriksa keberadaan sebuah database. Ini dilakukan dengan mencoba membuka direktori yang sesuai dengan nama database yang diberikan sebagai argumen. Jika direktori berhasil dibuka, menandakan bahwa database tersebut ada, dan fungsi mengembalikan true. Sebaliknya, jika direktori tidak bisa dibuka, maka database dianggap tidak ada, dan fungsi mengembalikan false. Dengan demikian, fungsi ini mengecek apakah direktori database dengan nama tertentu ada atau tidak di dalam struktur direktori "databases".

```
// Check if table exist
bool tbExist(char *db, char *tb)
{
    char path[256];
    sprintf(path, "%s/%s/%s.tb", "databases", db, tb);
    FILE *file = fopen(path, "r");
    if (file)
    {
        fclose(file);
        return true;
    }
    return false;
}
```
fungsi ini digunakan untuk memeriksa keberadaan file tabel yang spesifik di dalam struktur direktori "databases" dan di dalam database yang ditentukan.
```
// Check if string is in column
bool isInCol(char str[64], const char arr[128][64], int arr_size)
{
    for (int i = 0; i < arr_size; i++)
    {
        if (strcmp(str, arr[i]) == 0)
            return 1;
    }
    return false;
}
```
fungsi ini memeriksa keberadaan string di dalam kolom yang diwakili oleh array dua dimensi arr. Jika string yang dicari ditemukan di dalam kolom, fungsi akan memberikan nilai true.
```
//-------Check commands from client-------//

// Check if char is alphanumeric
bool alphanum(char c)
{
    if (c >= 'A' && c <= 'Z')
        return true;
    else if (c >= 'a' && c <= 'z')
        return true;
    else if (c >= '0' && c <= '9')
        return true;
    else if (c == '*' || c == '=')
        return true;
    return false;
}
```
fungsi ini berguna untuk mengecek apakah suatu karakter termasuk dalam kategori alfanumerik atau spesial karakter yang didefinisikan, dan mengembalikan nilai true jika karakter tersebut memenuhi salah satu kriteria tersebut.
```
// Split commands given from client
void splitCommands(const char *buffer, char com[128][64], int *size)
{
    int i = 0, j = 0, k = 0;
    while (i < strlen(buffer))
    {
        if (!alphanum(buffer[i]))
        {
            if (k)
            {
                com[j][k++] = '\0';
                j++;
                k = 0;
            }
        }
        else
            com[j][k++] = buffer[i];

        i++;
    }
    if (k)
    {
        com[j][k++] = '\0';
        j++;
        k = 0;
    }
    *size = j;
}
```
fungsi ini membagi perintah-perintah yang diterima dari klien menjadi token-token terpisah berdasarkan kriteria karakter pemisah yang ditentukan oleh fungsi alphanum. Setiap token (perintah) disimpan dalam array com dan ukuran dari perintah yang terpisah disimpan dalam variabel size.
```
//-------Data Deifinition Language-------//

// CREATE DATABASE
void createDB(char *db)
{
    mkdir("databases", 0777);
    char buffer[256];
    sprintf(buffer, "%s/%s", "databases", db);
    mkdir(buffer, 0777);
}
```
fungsi ini bertugas untuk menciptakan sebuah database baru dengan nama yang ditentukan dalam struktur direktori, memungkinkan penyimpanan data terorganisir dalam sistem file.
```
// CREATE TABLE
void createTB(char *db, char *tb, char *attribute[64], int size, char dt[128][32], int dt_size)
{
    char buffer[256];
    sprintf(buffer, "%s/%s/%s.tb", "databases", db, tb);

    FILE *file = fopen(buffer, "w");

    // File not found
    if (!file)
        return;

    for (int i = 0; i < size; i++)
    {
        fprintf(file, "%s,", attribute[i]);
    }
    fclose(file);
```
fungsi ini bertugas membuat sebuah file tabel baru dalam struktur direktori "databases", dengan menuliskan atribut-atribut tabel ke dalam file tersebut.
```
    // Create table information
    sprintf(buffer, "%s/%s/.%s_tb", "databases", db, tb);
    file = fopen(buffer, "w");
    if (file)
    {
        fprintf(file, "CREATE TABLE %s (", tb);
        for (int i = 0; i < dt_size; i++)
        {
            fprintf(file, "%s %s", attribute[i], dt[i]);
            if (i != dt_size - 1)
                fprintf(file, ", ");
        }

        fprintf(file, ");");
        fclose(file);
    }
}
```
fungsi ini bertugas membuat file informasi khusus untuk tabel yang baru saja dibuat dalam database. File ini berisi perintah pembuatan tabel dalam format yang sesuai dengan bahasa SQL, menyimpan informasi mengenai nama tabel dan atribut-atributnya.
```
//-------Data Manipulation Language-------//
void insertInto(char *db, char *tb, char *data[64], int size)
{
    char buffer[256];
    sprintf(buffer, "%s/%s/%s.tb", "databases", db, tb);
    FILE *file = fopen(buffer, "a");

    // File not exist
    if (!file)
        return;

    fprintf(file, "\n");
    for (int i = 0; i < size; i++)
    {
        fprintf(file, "%s,", data[i]);
    }
    fclose(file);
}
```
fungsi ini memungkinkan untuk menyisipkan data ke dalam sebuah tabel yang ada dalam struktur direktori "databases" di dalam database yang ditentukan.
```
// SELECT * FROM [table_name]
void selectAll(int *socket, const char *db, const char *tb)
{
    char buffer[256];
    sprintf(buffer, "%s/%s/%s.tb", "databases", db, tb);
    FILE *file = fopen(buffer, "r");
    if (!file)
        return;

    char data[256], ch;
    int data_size = 0;

    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == '\n')
        {
            if (data_size)
            {
                data[data_size++] = '\n';
                data[data_size++] = '\0';
                send(*socket, data, strlen(data), 0);
                data_size = 0;
            }
        }
        else
            data[data_size++] = ch;
    }
    if (data_size)
    {
        data[data_size++] = '\n';
        data[data_size++] = '\0';
        send(*socket, data, strlen(data), 0);
        data_size = 0;
    }
    fclose(file);
}
```
fungsi ini memungkinkan untuk membaca semua data yang ada dalam sebuah tabel dalam struktur direktori "databases" dan mengirimkannya melalui soket ke penerima.
```
// SELECT * FROM [table_name] WHERE [condition]
void selectAllWhere(int *socket, const char *db, const char *tb, const char *col, const char *value)
{
    char buffer[256];
    sprintf(buffer, "%s/%s/%s.tb", "databases", db, tb);
    FILE *file = fopen(buffer, "r");
    if (!file)
        return;

    char ch, data[256], col_i[256];
    int data_size = 0, col_size_i = 0, col_number = 1, col_num_i = 1;
    bool isInHeader = true, colFound = false, row_valid = false;

    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == '\n')
        {
            col_num_i = 1;

            if (data_size)
            {
                data[data_size++] = '\n';
                data[data_size++] = '\0';
                if (row_valid || isInHeader)
                    send(*socket, data, strlen(data), 0);
                data_size = 0;
            }
            isInHeader = false;
            row_valid = false;
        }
        else
        {
            if (ch == ',')
            {
                col_i[col_size_i] = '\0';
                col_size_i = 0;

                if (col_num_i == col_number)
                {
                    if (!isInHeader && colFound)
                    {
                        if (strcmp(value, col_i) == 0)
                            row_valid = true;
                    }
                }
                col_num_i++;

                if (isInHeader && !colFound)
                {
                    if (strcmp(col, col_i) == 0)
                        colFound = true;
                    else
                        col_number++;
                }
                memset(col_i, 0, sizeof(col_i));
            }
            else
                col_i[col_size_i++] = ch;

            data[data_size++] = ch;
        }
    }

    if (data_size)
    {
        data[data_size++] = '\n';
        data[data_size++] = '\0';
        if (row_valid)
            send(*socket, data, strlen(data), 0);
        data_size = 0;
    }

    fclose(file);
}
```
fungsi ini memungkinkan untuk membaca baris-baris dari sebuah tabel yang memenuhi suatu kondisi tertentu dan mengirimkan baris-baris tersebut melalui soket ke penerima.
```
// SELECT [column1, column2, ......] FROM [table_name]
void selectColFrom(int *socket, const char *db, const char *tb, const char col[128][64], int col_size) {
    char buffer[256];
    sprintf(buffer, "%s/%s/%s.tb", "databases", db, tb);
    FILE *file = fopen(buffer, "r");
    if (!file)
        return;

    char data[4096], tb_col[64], tb_col_size = 0;
    bool col_reserved[128];

    memset(col_reserved, 0, sizeof(col_reserved));
    memset(data, 0, sizeof(data));

    int col_num = 0;
    char ch;

    // Read header
    while (fscanf(file, "%c", &ch) != EOF) {
        if (ch == ',') {
            tb_col[tb_col_size++] = '\0';
            if (isInCol(tb_col, col, col_size)) {
                col_reserved[col_num] = 1;

                if (col_size == 1) {
                    strcat(data, tb_col);
                    strcat(data, " "); // Tambahkan spasi setelah satu kolom
                } else {
                    strcat(data, tb_col);
                    strcat(data, " ");
                }
            }
            tb_col[0] = '\0';
            tb_col_size = 0;
            col_num++;
        } else if (ch == '\n') {
            strcat(data, "\n");
            send(*socket, data, strlen(data), 0);
            break;
        } else {
            tb_col[tb_col_size++] = ch;
        }
    }

    col_num = 0;
    memset(data, 0, sizeof(data));

    // Read data
    while (fscanf(file, "%c", &ch) != EOF) {
        if (ch == ',') {
            tb_col[tb_col_size++] = '\0';
            if (col_reserved[col_num++]) {
                strcat(data, tb_col);
                strcat(data, " ");
            }
            tb_col[0] = '\0';
            tb_col_size = 0;
        } else if (ch == '\n') {
            col_num = 0;
            strcat(data, "\n");
            send(*socket, data, strlen(data), 0);
            memset(data, 0, sizeof(data));
        } else {
            tb_col[tb_col_size++] = ch;
        }
    }
    strcat(data, "\n");
    send(*socket, data, strlen(data), 0);
    memset(data, 0, sizeof(data));
    fclose(file);
}
```
Fungsi selectColFrom membaca data dari sebuah file yang merepresentasikan tabel dalam format kolom dan baris. Fungsi ini memproses kolom yang diminta dari tabel yang ditentukan, mengumpulkan data yang sesuai, dan mengirimkannya melalui socket yang disediakan. Pertama, fungsi ini membaca header (nama kolom) dari file, memeriksa kolom yang diminta, lalu mengirim header ke socket. Selanjutnya, fungsi membaca data (baris-baris), mengumpulkan data yang sesuai dengan kolom yang diminta, dan mengirimnya ke socket.
```
// SELECT [column1, column2, ......] FROM [table_name] WHERE [column=value]
void selectColFromWhere(int *socket, const char *db, const char *tb, const char col[128][64], int col_size, const char *where_col, const char *val)
{
    char buff[256];
    sprintf(buff, "%s/%s/%s.tb", "databases", db, tb);
    FILE *file = fopen(buff, "r");
    if (!file)
        return;

    char data[4096], tb_col[64], tb_col_size = 0;
    bool tb_col_reserved[128];

    memset(tb_col_reserved, 0, sizeof(tb_col_reserved));
    memset(data, 0, sizeof(data));

    int tb_col_number = 0;
    char ch, cell_name_i[256];
    int cell_name_size_i = 0, col_number = 1;
    bool col_found = false;

    // Read header
    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[tb_col_size++] = '\0';
            cell_name_i[cell_name_size_i] = '\0';
            cell_name_size_i = 0;

            // Check if condition column is found
            if (!col_found)
            {
                if (strcmp(cell_name_i, where_col) == 0)
                    col_found = true;
                else
                    col_number++;
            }

            if (isInCol(tb_col, col, col_size))
            {
                tb_col_reserved[tb_col_number] = 1;
                if (col_size == 1)
                {
                    strcat(data, tb_col);
                    strcat(data, " ");
                }
                else
                {
                    strcat(data, tb_col);
                    strcat(data, " ");
                }
            }
            tb_col[0] = '\0';
            tb_col_size = 0;
            tb_col_number++;

            memset(cell_name_i, 0, sizeof(cell_name_i));
        }
        else if (ch == '\n')
        {
            strcat(data, "\n");
            send(*socket, data, strlen(data), 0);
            break;
        }
        else
        {
            tb_col[tb_col_size++] = ch;
            cell_name_i[cell_name_size_i++] = ch;
        }
    }

    tb_col_number = 0;
    memset(data, 0, sizeof(data));

    int col_num_i = 1;
    bool isValid = false;

    // Read content
    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[tb_col_size++] = '\0';
            cell_name_i[cell_name_size_i] = '\0';
            cell_name_size_i = 0;

            if (col_num_i == col_number)
            {
                if (strcmp(cell_name_i, val) == 0)
                    isValid = true;
            }

            if (tb_col_reserved[tb_col_number++])
            {
                strcat(data, tb_col);
                strcat(data, " ");
            }

            tb_col[0] = '\0';
            tb_col_size = 0;

            col_num_i++;
            memset(cell_name_i, 0, sizeof(cell_name_i));
        }
        else if (ch == '\n')
        {
            col_num_i = 1;
            tb_col_number = 0;
            strcat(data, "\n");
            if (isValid)
                send(*socket, data, strlen(data), 0);

            isValid = false;
            memset(data, 0, sizeof(data));
        }
        else
        {
            tb_col[tb_col_size++] = ch;
            cell_name_i[cell_name_size_i++] = ch;
        }
    }

    strcat(data, "\n");
    if (isValid)
        send(*socket, data, strlen(data), 0);

    memset(data, 0, sizeof(data));
    fclose(file);
}
```
Fungsi selectColFromWhere digunakan untuk membaca data dari sebuah file yang mewakili tabel dengan format kolom dan baris. Fungsi ini memproses kolom yang diminta dari tabel yang ditentukan, dengan mempertimbangkan kondisi yang diberikan dalam bentuk "WHERE column=value". Pertama, fungsi ini membaca header (nama kolom) dari file, menandai kolom yang diminta, dan mengirim header ke socket. Selanjutnya, fungsi membaca data (baris-baris), memeriksa nilai dalam kolom yang sesuai dengan kondisi "WHERE", mengumpulkan data yang cocok, dan mengirimkannya melalui socket.
```
// UPDATE [table_name] SET [column = value]
int updateSet(const char *db, const char *tb, char col[64], char val[])
{
    char buffer[256], new_buffer[256];
    sprintf(buffer, "%s/%s/%s.tb", "databases", db, tb);
    sprintf(new_buffer, "%s/%s/%s_updated.tb", "databases", db, tb);

    FILE *file = fopen(buffer, "r");
    if (!file)
        return 0;
    FILE *new_file = fopen(new_buffer, "w");
    if (!new_file)
        return 0;

    char tb_col[64];
    int col_size = 0, col_id = 0, col_num = 0;
    char ch;

    // Read header
    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[col_size++] = '\0';
            if (strcmp(tb_col, col) == 0)
                col_id = col_num;

            fprintf(new_file, "%s,", tb_col);
            tb_col[0] = '\0';
            col_size = 0;
            col_num++;
        }

        else if (ch == '\n')
        {
            fprintf(new_file, "\n");
            break;
        }

        else
            tb_col[col_size++] = ch;
    }
    col_num = 0;

    // Read content
    int affected = 0;
    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[col_size++] = '\0';
            if ((col_num++) == col_id)
            {
                fprintf(new_file, "%s,", val);
                affected++;
            }
            else
            {
                fprintf(new_file, "%s,", tb_col);
            }
            tb_col[0] = '\0';
            col_size = 0;
        }
        else if (ch == '\n')
        {
            col_num = 0;
            fprintf(new_file, "\n");
        }
        else
            tb_col[col_size++] = ch;
    }

    fclose(new_file);
    fclose(file);

    remove(buffer);
    rename(new_buffer, buffer);
    return affected;
}
```
Fungsi updateSet ini bertujuan untuk memperbarui nilai dalam suatu kolom pada file yang merepresentasikan tabel. Fungsi ini menerima nama database, nama tabel, nama kolom, dan nilai baru yang akan dimasukkan ke dalam kolom tersebut. Pertama, fungsi membuka file yang berisi data tabel dan sebuah file baru untuk menulis data yang telah diperbarui. Selanjutnya, fungsi membaca header (nama kolom) dari file asli, mencari kolom yang sesuai dengan yang diminta, dan menulis ulang data ke file baru dengan nilai yang diperbarui sesuai dengan kolom yang ditentukan. Jumlah baris yang terpengaruh oleh perubahan ini dihitung dan dikembalikan sebagai output fungsi setelah file-file ditutup dan perubahan disimpan dengan menghapus file lama dan menamai file baru dengan nama file lama.
```
// UPDATE [table_name] SET [column = value] WHERE [column = value]
int updateSetWhere(const char *db, const char *tb, char col[64], char val[], char *old_val, char *where_col)
{
    char buffer[256], new_buffer[256];
    sprintf(buffer, "%s/%s/%s.tb", "databases", db, tb);
    sprintf(new_buffer, "%s/%s/%s_updated.tb", "databases", db, tb);

    FILE *file = fopen(buffer, "r");
    if (!file)
        return 0;
    FILE *new_file = fopen(new_buffer, "w");
    if (!new_file)
        return 0;

    char tb_col[64], ch;
    int col_size = 0, col_id = 0, col_num = 0;

    // Read header
    char col_i[256];
    int col_size_i = 0, col_number = 1;
    bool col_found = false;
    memset(col_i, 0, sizeof(col_i));

    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[col_size++] = '\0';
            col_i[col_size_i] = '\0';
            if ((strcmp(col_i, where_col) == 0) && !col_found)
                col_found = true;

            if (strcmp(tb_col, col) == 0)
                col_id = col_num;
            fprintf(file, "%s,", tb_col);
            tb_col[0] = '\0';
            col_size = 0;
            col_num++;

            if (!col_found)
                col_number++;
            col_size_i = 0;
            memset(col_i, 0, sizeof(col_i));
        }
        else if (ch == '\n')
        {
            fprintf(new_file, "\n");
            break;
        }
        else
        {
            tb_col[col_size++] = ch;
            col_i[col_size_i++] = ch;
        }
    }
    col_num = 0;

    // Read content
    col_size_i = 0;
    int col_num_i = 1;
    memset(col_i, 0, sizeof(col_i));
    char temp_cell[256];
    memset(temp_cell, 0, sizeof(temp_cell));
    int affected = 0;
    bool isValid = false;
    char data[256];
    char invalid_data[256];

    memset(data, 0, sizeof(data));
    memset(invalid_data, 0, sizeof(invalid_data));

    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[col_size++] = '\0';
            col_i[col_size_i] = '\0';

            if ((strcmp(old_val, col_i) == 0) && (col_number == col_num_i))

                isValid = true;

            if (((col_num++) == col_id))
            {
                sprintf(temp_cell, "%s,", val);
                strcat(data, temp_cell);
            }
            else
            {
                sprintf(temp_cell, "%s,", tb_col);
                strcat(data, temp_cell);
            }

            sprintf(temp_cell, "%s,", tb_col);
            strcat(invalid_data, temp_cell);

            tb_col[0] = '\0';
            col_size = 0;

            col_size_i = 0;
            col_num_i++;
            memset(col_i, 0, sizeof(col_i));
        }
        else if (ch == '\n')
        {
            // New line
            if (isValid)
            {
                fprintf(new_file, "%s", data);
                fprintf(new_file, "\n");
                affected++;
            }
            else
            {
                fprintf(new_file, "%s", invalid_data);
                fprintf(new_file, "\n");
            }

            memset(data, 0, sizeof(data));
            memset(invalid_data, 0, sizeof(invalid_data));
            col_num_i = 1;
            col_num = 0;
            isValid = false;
        }
        else
        {
            tb_col[col_size++] = ch;
            col_i[col_size_i++] = ch;
        }
    }

    if (isValid)
    {
        fprintf(new_file, "%s", data);
        affected++;
    }
    else
        fprintf(new_file, "%s", invalid_data);
    fclose(new_file);
    fclose(file);
    remove(buffer);
    rename(new_buffer, buffer);
    return affected;
}
```
Fungsi updateSetWhere bertujuan untuk memperbarui nilai dalam suatu kolom pada file yang merepresentasikan tabel dengan mempertimbangkan kondisi tertentu yang diberikan dalam bentuk "WHERE column = value". Fungsi ini menerima nama database, nama tabel, nama kolom yang akan diperbarui, nilai baru yang akan dimasukkan ke dalam kolom tersebut, nilai lama yang akan diubah, dan nama kolom untuk kondisi "WHERE". Pertama, fungsi membuka file yang berisi data tabel dan sebuah file baru untuk menulis data yang telah diperbarui. Selanjutnya, fungsi membaca header (nama kolom) dari file asli, menemukan kolom yang sesuai dengan yang diminta dan kondisi "WHERE", kemudian menulis ulang data ke file baru dengan nilai yang diperbarui sesuai dengan kolom yang ditentukan. Fungsi ini menghitung jumlah baris yang terpengaruh oleh perubahan ini dan mengembalikan nilai tersebut setelah file-file ditutup dan perubahan disimpan dengan menghapus file lama dan menamai file baru dengan nama file lama.
```
// DELETE FROM
int deleteFrom(char *db, char *tb, char col[], char val[])
{
    char buffer[256], new_buffer[256];
    sprintf(buffer, "%s/%s/%s.tb", "databases", db, tb);
    sprintf(new_buffer, "%s/%s/%s_updated.tb", "databases", db, tb);

    FILE *file = fopen(buffer, "r");
    if (!file)
        return 0;
    FILE *new_file = fopen(new_buffer, "w");
    if (!new_file)
        return 0;

    char tb_col[64], ch;
    int col_size = 0, col_id = 0, col_num = 0;
    bool where = true;

    // '#' means query is not using "WHERE"
    if (strcmp(col, "#") == 0)
        where = false;

    // Read header
    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[col_size++] = '\0';
            if (where && strcmp(tb_col, col) == 0)
                col_id = col_num;

            fprintf(new_file, "%s,", tb_col);
            tb_col[0] = '\0';
            col_size = 0;
            col_num++;
        }
        else if (ch == '\n')
        {
            // New line is detected
            fprintf(new_file, "\n");
            break;
        }
        else
            tb_col[col_size++] = ch;
    }
    col_num = 0;

    // Read content
    int affected = 0;
    char data[4096];
    memset(data, 0, sizeof(data));
    bool deleted = false;
    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[col_size++] = '\0';
            if (!where)
            {
                deleted = true;
                if ((col_num++) == col_id)
                    affected++;
            }
            else
            {
                if ((col_num++) == col_id && strcmp(tb_col, val) == 0)
                {
                    deleted = true;
                    affected++;
                }
            }
            strcat(data, tb_col);
            strcat(data, ",");
            tb_col[0] = '\0';
            col_size = 0;
        }
        else if (ch == '\n')
        {
            // New line
            col_num = 0;
            strcat(data, "\n");
            if (!deleted)
            {
                fprintf(new_file, "%s", data);
            }
            memset(data, 0, sizeof(data));
            deleted = false;
        }
        else
            tb_col[col_size++] = ch;
    }

    strcat(data, "\n");
    if (!deleted)
        fprintf(new_file, "%s", data);

    memset(data, 0, sizeof(data));
    deleted = false;
    fclose(new_file);
    fclose(file);
    remove(buffer);
    rename(new_buffer, buffer);
    return affected;
}
```
Fungsi deleteFrom digunakan untuk menghapus baris-baris tertentu dari file yang mewakili tabel dengan mempertimbangkan kondisi yang diberikan dalam bentuk "DELETE FROM [table_name] WHERE [column = value]". Fungsi ini menerima nama database, nama tabel, nama kolom yang akan dijadikan kondisi, dan nilai yang akan dibandingkan dalam kolom tersebut untuk menghapus baris. Pertama, fungsi membuka file yang berisi data tabel dan sebuah file baru untuk menulis data yang akan diperbarui setelah penghapusan. Selanjutnya, fungsi membaca header (nama kolom) dari file asli dan menemukan kolom yang sesuai dengan yang diminta. Kemudian, fungsi membaca data (baris-baris), memeriksa kondisi yang diberikan, dan menulis ulang data ke file baru, kecuali baris yang memenuhi kondisi untuk dihapus. Fungsi ini menghitung jumlah baris yang terpengaruh oleh penghapusan dan mengembalikan nilai tersebut setelah file-file ditutup dan perubahan disimpan dengan menghapus file lama dan menamai file baru dengan nama file lama.
```
// Directory traversal to find correct database
int findDB(const char *path)
{
    DIR *dir = opendir(path);
    size_t path_len = strlen(path);
    int r = -1;

    if (dir)
    {
        struct dirent *dp;

        r = 0;
        while (!r && (dp = readdir(dir)))
        {
            int r2 = -1;
            char *buffer;
            size_t length;

            if (!strcmp(dp->d_name, ".") || !strcmp(dp->d_name, ".."))
                continue;

            length = path_len + strlen(dp->d_name) + 2;
            buffer = malloc(length);

            if (buffer)
            {
                struct stat statbuf;

                snprintf(buffer, length, "%s/%s", path, dp->d_name);
                if (!stat(buffer, &statbuf))
                {
                    if (S_ISDIR(statbuf.st_mode))
                        r2 = findDB(buffer);
                    else
                        r2 = unlink(buffer);
                }
                free(buffer);
            }
            r = r2;
        }
        closedir(dir);
    }

    if (!r)
        r = rmdir(path);

    return r;
}

void dropDB(char *db)
{
    char path[50];
    sprintf(path, "%s/%s", "databases", db);
    findDB(path);
}
```
Fungsi findDB bertanggung jawab untuk melakukan pencarian dan penghapusan seluruh struktur direktori terkait dengan suatu database dalam sistem file. Melalui rekursi, fungsi ini menelusuri setiap direktori dan file di jalur yang diberikan, menggunakan unlink untuk menghapus file-file dan rmdir untuk menghapus direktori setelah semua entri di dalamnya terhapus. Dipanggil dari fungsi dropDB, yang membentuk jalur direktori berdasarkan nama database yang ingin dihapus, proses ini dapat menghapus seluruh data terkait suatu database, namun harus digunakan dengan hati-hati karena bersifat permanen dan tidak dapat dipulihkan.
```
// DROP TABLE [table_name]
void dropTB(int *socket, char *db, char *tb)
{
    char path[50];
    sprintf(path, "%s/%s/%s.tb", "databases", db, tb);
    char message[1024];

    if (remove(path) == 0)
    {
        sprintf(message, "Table %s.%s has been dropped.\n", db, tb);
        send(*socket, message, strlen(message), 0);
    }
    else
    {
        strcpy(message, "Error! Table failed to drop\n");
        send(*socket, message, strlen(message), 0);
    }
}
```
Fungsi dropTB bertujuan untuk menghapus sebuah tabel dari database dengan menghapus file yang merepresentasikan tabel tersebut di sistem file. Fungsi ini menerima nama database dan nama tabel, membentuk jalur file yang sesuai dengan lokasi tabel tersebut, dan menggunakan fungsi remove untuk menghapus file tersebut dari sistem file. Jika penghapusan berhasil, fungsi mengirimkan pesan konfirmasi ke socket yang diberikan. Namun, jika penghapusan gagal, fungsi juga mengirimkan pesan error ke socket yang sama.
```
// DROP COLUMN
int dropColumn(const char *db, const char *tb, char col[64], char val[])
{
    char buffer[256], new_buffer[256];
    sprintf(buffer, "%s/%s/%s.tb", "databases", db, tb);
    sprintf(new_buffer, "%s/%s/%s_updated.tb", "databases", db, tb);
    FILE *file = fopen(buffer, "r");
    if (!file)
        return 0;
    FILE *new_file = fopen(new_buffer, "w");
    if (!new_file)
        return 0;

    char tb_col[64], ch;
    int col_size = 0, col_id = 0, col_num = 0;

    // Read header
    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[col_size++] = '\0';

            if (strcmp(tb_col, col) == 0)
            {
                col_id = col_num;
                fprintf(new_file, "%s", val);
            }
            else
                fprintf(new_file, "%s,", tb_col);

            tb_col[0] = '\0';
            col_size = 0;
            col_num++;
        }
        else if (ch == '\n')
        {
            fprintf(new_file, "\n");
            break;
        }
        else
            tb_col[col_size++] = ch;
    }
    col_num = 0;

    // Read content
    int affected = 0;
    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[col_size++] = '\0';
            if ((col_num++) == col_id)
            {
                fprintf(new_file, "%s", val);
                affected++;
            }
            else
            {
                fprintf(new_file, "%s,", tb_col);
            }
            tb_col[0] = '\0';
            col_size = 0;
        }
        else if (ch == '\n')
        {
            col_num = 0;
            fprintf(new_file, "\n");
        }
        else
            tb_col[col_size++] = ch;
    }
    fclose(new_file);
    fclose(file);
    remove(buffer);
    rename(new_buffer, buffer);
    return affected;
}
```
Fungsi dropColumn dirancang untuk menghapus sebuah kolom dari sebuah tabel di dalam database. Fungsi ini menerima nama database, nama tabel, nama kolom yang akan dihapus, dan nilai default yang akan digunakan untuk menggantikan data yang ada pada kolom tersebut. Fungsi membuka file yang merepresentasikan tabel, membaca header (nama kolom), dan menulis ulang isi tabel ke file baru dengan mengabaikan kolom yang ingin dihapus, serta menulis nilai default pada posisi yang sesuai. Jika proses penghapusan kolom berhasil, fungsi mengembalikan jumlah baris yang terpengaruh oleh perubahan ini. Namun, jika proses ini gagal atau tidak ada baris yang terpengaruh, fungsi mengembalikan nilai 0.

```
//-------Authentication-------//

// Set current user
void setUser(User *user, char *username, char *password)
{
    strcpy(user->username, username);
    strcpy(user->password, password);
    return;
}

void readUserTB(User *user, char *line)
{
    char username[50];
    char password[50];

    int i = 0, j = 0;

    while (line[i] != ',')
        username[j++] = line[i++];

    username[j++] = '\0';
    i++;
    strcpy(user->username, username);

    j = 0;
    while (line[i] != ',')
    {
        password[j++] = line[i++];
    }
    password[j++] = '\0';
    strcpy(user->password, password);
}
```
Fungsi setUser digunakan untuk mengatur informasi pengguna saat ini dengan menyimpan nama pengguna (username) dan kata sandi (password) ke dalam struktur data User.<br>
<br>
Fungsi readUserTB dirancang untuk membaca baris yang mewakili pengguna dari file tabel pengguna (user table). Fungsi ini mengambil sebuah baris sebagai input, kemudian membaca dan memisahkan nama pengguna (username) dan kata sandi (password) dari baris tersebut, menyimpannya ke dalam struktur User. Masing-masing data dipisahkan oleh karakter koma (',') di dalam baris file tabel pengguna.
```
// Check if user is equal
bool isEqual(User *a, User *b)
{
    return (strcmp(a->username, b->username) == 0 && strcmp(a->password, b->password) == 0);
}
```
Fungsi isEqual digunakan untuk memeriksa apakah dua entitas pengguna (user) sama atau tidak. Fungsi ini membandingkan nama pengguna (username) dan kata sandi (password) dari dua struktur data User, a dan b, dan mengembalikan nilai boolean true jika keduanya sama persis, atau false jika terdapat perbedaan dalam salah satu atau kedua atribut pengguna tersebut.
```
// Check if user and password match
bool scanUser(User *user)
{
    char path[256];
    sprintf(path, "%s/%s/%s.tb", "databases", "userDB", "users");
    FILE *file = fopen(path, "r");

    if (!file)
        return false;

    User check;
    char temp[256];
    int t_size = 0;
    char ch;
    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == '\n')
        {
            if (t_size)
            {
                temp[t_size++] = '\n';
                temp[t_size++] = '\0';
                readUserTB(&check, temp);
                if (isEqual(user, &check))
                    return true;

                t_size = 0;
            }
        }
        else
            temp[t_size++] = ch;
    }

    if (t_size)
    {
        temp[t_size++] = '\n';
        temp[t_size++] = '\0';
        readUserTB(&check, temp);
        if (isEqual(user, &check))
            return true;

        t_size = 0;
    }
    fclose(file);

    return false;
}
```
Fungsi scanUser bertujuan untuk memindai file tabel pengguna (users) dalam database pengguna (userDB) untuk memeriksa apakah terdapat kesesuaian antara informasi pengguna (username dan password) yang diberikan dengan data yang ada di dalam tabel tersebut. Fungsi membuka file tabel pengguna, kemudian secara berurutan membaca setiap baris dari file tersebut. Setiap baris diproses dan dibandingkan dengan informasi pengguna yang diberikan melalui fungsi readUserTB dan isEqual. Jika terdapat kesamaan (match) antara pengguna yang diberikan dengan entri yang ada dalam tabel, fungsi mengembalikan nilai true. Jika tidak ditemukan kesesuaian setelah proses membaca seluruh entri, fungsi mengembalikan nilai false.
```
// Check current user
bool checkUser(User *user)
{
    if (strcmp(user->username, "root") == 0)
    {
        user->isRoot = true;
        return true;
    }
    else if (scanUser(user))
    {
        user->isRoot = false;
        return true;
    }

    return false;
}
```
Fungsi checkUser bertujuan untuk memeriksa keberadaan dan status pengguna yang saat ini login. Fungsi ini memiliki tiga tahap pengecekan. Pertama, jika pengguna adalah "root", fungsi akan menandai pengguna sebagai root dan mengembalikan nilai true, memberikan hak akses penuh pada sistem. Kedua, jika bukan "root", fungsi memanggil scanUser untuk memeriksa kesesuaian pengguna dengan entri yang ada dalam tabel pengguna. Jika ada kesesuaian, pengguna dianggap valid dan fungsi mengembalikan nilai true. Jika tidak ada kesesuaian pada tahap kedua, fungsi mengembalikan nilai false, menandakan bahwa pengguna tidak valid atau tidak terdaftar.
```
// Grant access to database
void grantPermission(char *db, char *username)
{
    char *attribute[64];
    attribute[0] = db;
    attribute[1] = username;
    insertInto("userDB", "permission", attribute, 2);
}

void __hasPermissionToDBHelper(char *line, char *db_r, char *us_r)
{
    char db[50], user[50];
    int i = 0, j = 0;

    while (line[i] != ',')
        db[j++] = line[i++];
    db[j++] = '\0';

    i++;
    strcpy(db_r, db);
    j = 0;

    while (line[i] != ',')
        user[j++] = line[i++];
    user[j++] = '\0';

    strcpy(us_r, user);
}
```
Fungsi grantPermission bertujuan untuk memberikan izin akses ke sebuah database kepada seorang pengguna tertentu. Fungsi ini menerima nama database (db) dan nama pengguna (username), lalu menyimpan informasi tersebut ke dalam sebuah array attribute dan menggunakan fungsi insertInto untuk menambahkan entri baru ke dalam tabel permission pada database userDB.<br>
<br>
Fungsi __hasPermissionToDBHelper adalah fungsi pembantu yang membaca dan memproses baris data dari tabel permission pada database userDB. Fungsi ini memisahkan nama database dan nama pengguna dari sebuah baris, kemudian menyimpan informasi tersebut dalam variabel db_r dan us_r masing-masing untuk digunakan dalam proses pengecekan izin akses ke database tertentu.
```
// Check if user has permission to database
bool isAllowedToDB(char *username, char *db)
{
    // Root has permission to all databases (except userDB)
    if (strcmp(username, "root") == 0)
        return true;

    char path[256];
    sprintf(path, "%s/%s/%s.tb", "databases", "userDB", "permission");
    FILE *file = fopen(path, "r");

    // File not exist
    if (!file)
        return false;

    int temp_size = 0;
    char temp[256], ch, db_read[50], us_read[50];

    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == '\n')
        {
            if (temp_size)
            {
                temp[temp_size++] = '\n';
                temp[temp_size++] = '\0';
                __hasPermissionToDBHelper(temp, db_read, us_read);

                // Database and username match
                if (strcmp(db_read, db) == 0 && strcmp(us_read, username) == 0)
                    return true;

                temp_size = 0;
            }
        }
        else
            temp[temp_size++] = ch;
    }

    if (temp_size)
    {
        temp[temp_size++] = '\n';
        temp[temp_size++] = '\0';
        __hasPermissionToDBHelper(temp, db_read, us_read);

        //
        if (strcmp(db_read, db) == 0 && strcmp(us_read, username) == 0)
            return true;

        temp_size = 0;
    }
    fclose(file);

    return false;
}
```
Fungsi isAllowedToDB bertujuan untuk memeriksa apakah seorang pengguna memiliki izin akses ke suatu database tertentu. Pertama, fungsi memeriksa apakah pengguna adalah "root". Jika iya, maka pengguna "root" memiliki izin akses ke semua database kecuali database "userDB", sehingga fungsi mengembalikan nilai true.<br>
<br>
Untuk pengguna non-"root", fungsi membuka file yang berisi daftar izin akses pada tabel permission dari database userDB. Fungsi ini membaca baris-baris pada file tersebut, memisahkan nama database dan nama pengguna, kemudian membandingkan dengan input yang diberikan (nama database dan nama pengguna). Jika ditemukan kecocokan antara nama database dan nama pengguna dalam file permission, fungsi mengembalikan nilai true yang menandakan bahwa pengguna memiliki izin akses ke database tersebut. Jika tidak ada kecocokan yang ditemukan setelah membaca seluruh entri pada file, fungsi mengembalikan nilai false yang menunjukkan bahwa pengguna tidak memiliki izin akses ke database tersebut.
```
// Create new user
void createUser(char *username, char *password)
{
    char *attribute[64];
    attribute[0] = username;
    attribute[1] = password;
    insertInto("userDB", "users", attribute, 2);
}
```
Fungsi createUser bertujuan untuk membuat pengguna baru dalam sistem. Fungsi ini menerima parameter berupa nama pengguna (username) dan kata sandi (password). Informasi ini disimpan ke dalam array attribute, kemudian digunakan dalam pemanggilan fungsi insertInto untuk menambahkan entri baru ke dalam tabel users pada database userDB. Dengan ini, fungsi createUser bertanggung jawab untuk menambahkan pengguna baru ke dalam basis data pengguna (userDB).
```
//-------Log-------//

// Get Current Time
char *getCurTime()
{
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char *time;
    time = (char *)malloc(100);
    sprintf(time, "%d-%02d-%02d %02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    return time;
}
```
Fungsi getCurTime digunakan untuk mendapatkan waktu saat ini dalam format yang ditentukan. Fungsi ini menggunakan fungsi time dari C untuk mendapatkan waktu saat ini dalam detik dari epoch, kemudian mengonversinya ke dalam format waktu lokal menggunakan localtime. Setelah itu, waktu lokal tersebut diformat sesuai dengan format yang diinginkan menggunakan sprintf dan disimpan dalam sebuah string yang dialokasikan secara dinamis. String waktu tersebut kemudian dikembalikan agar dapat digunakan untuk keperluan logging atau pencatatan waktu pada aplikasi.
```
// Write log
void writeLog(User *user, char *command)
{
    char currTime[30];
    strcpy(currTime, getCurTime());

    char log[1000];
    sprintf(log, "%s:%s:%s", currTime, user->username, command);

    FILE *file = fopen("database.log", "a");
    fprintf(file, "%s\n", log);
    fclose(file);
}
```
Fungsi writeLog menerima informasi pengguna dan perintah yang dijalankan, menggunakan getCurTime untuk mendapatkan waktu saat ini, dan menggabungkan informasi tersebut ke dalam satu string log. Dengan membuka file database.log dalam mode append, fungsi menulis log kegiatan ke file tersebut, memisahkan setiap entri dengan newline. Ini memungkinkan pencatatan aktivitas pengguna untuk pemantauan dan audit trail pada aplikasi database.
```
//-------Dump-------//

// Scan table
void scanTB(int *socket, char db[], char tb[])
{
    char path[256];
    sprintf(path, "%s/%s/%s.tb", "databases", db, tb);
    FILE *file = fopen(path, "r");
    if (file)
    {
        char buffer[2048], data[1024], temp[64], ch;
        int t_size = 0;
        int new_socket = *(int *)socket;

        memset(data, 0, sizeof(data));

        // Read table information file
        char createPath[256];
        sprintf(createPath, "%s/%s/.%s_tb", "databases", db, tb);

        FILE *createFile = fopen(createPath, "r");
        if (createFile)
        {
            memset(data, 0, sizeof(data));
            int i = 0;
            while (fscanf(createFile, "%c", &ch) != EOF)
                data[i++] = ch;

            data[i++] = '\0';
            sprintf(buffer, "%s\n", data);
            memset(data, 0, sizeof(data));

            send(new_socket, buffer, strlen(buffer), 0);
            fclose(createFile);
        }

        // Read header
        while (fscanf(file, "%c", &ch) != EOF)
        {
            if (ch == '\n')
                break;
        }

        memset(data, 0, sizeof(data));

        strcpy(data, "INSERT INTO ");
        strcat(data, tb);
        strcat(data, " (");

        // Read content
        while (fscanf(file, "%c", &ch) != EOF)
        {
            if (ch == ',')
            {
                temp[t_size++] = '\0';
                strcat(data, temp);
                strcat(data, ", ");

                temp[0] = '\0';
                t_size = 0;
            }

            else if (ch == '\n')
            {
                // Replace ", " with ");"
                data[strlen(data) - 1] = ';';
                data[strlen(data) - 2] = ')';

                sprintf(buffer, "%s\n", data);
                send(new_socket, buffer, strlen(buffer), 0);

                memset(data, 0, sizeof(data));

                strcpy(data, "INSERT INTO ");
                strcat(data, tb);
                strcat(data, " (");
            }
            else
                temp[t_size++] = ch;
        }

        // Replace ", " with ");"
        data[strlen(data) - 1] = ';';
        data[strlen(data) - 2] = ')';

        sprintf(buffer, "%s\n", data);
        send(new_socket, buffer, strlen(buffer), 0);

        memset(data, 0, sizeof(data));

        fclose(file);
    }
}
```
Fungsi scanTB melakukan pemindaian tabel, membaca data dari file tabel, dan mengirimkannya ke socket yang ditentukan. Proses ini melibatkan membaca struktur file tabel untuk mengonstruksi pernyataan INSERT INTO yang merepresentasikan isi tabel. Data yang dibaca dari file tabel dibentuk ke dalam pernyataan SQL dan dikirimkan melalui socket, memungkinkan untuk ekstraksi data dari tabel dalam format yang dapat diimpor ke dalam database lain atau alat manajemen basis data yang sesuai.
```
// Scan database
void scanDB(int *socket, char db[])
{
    DIR *dir;
    struct dirent *dp;

    char path[256];
    sprintf(path, "%s/%s", "databases", db);

    dir = opendir(path);

    if (dir)
    {
        int new_socket = *(int *)socket;

        char buffer[1024];
        memset(buffer, 0, sizeof(buffer));
        sprintf(buffer, "CREATE DATABASE %s;\n", db);

        send(new_socket, buffer, strlen(buffer), 0);

        // Directory traversal
        while ((dp = readdir(dir)))
        {
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
            {
                char tb[64];
                strcpy(tb, dp->d_name);

                // Remove .tb extension from string table_name
                tb[strlen(tb) - 3] = '\0';
                scanTB(socket, db, tb);
            }
        }

        close(new_socket);
        closedir(dir);
    }
}
```
Fungsi scanDB melakukan pemindaian keseluruhan database dengan membuka direktori yang sesuai dengan nama database. Proses ini memindai setiap tabel di dalamnya dengan menggunakan fungsi scanTB untuk mengekstrak data dari setiap tabel dan mengirimkannya melalui socket yang ditentukan. Selain itu, fungsi ini juga mengirim pernyataan SQL CREATE DATABASE untuk menciptakan basis data yang sesuai dengan nama yang sedang diproses. Setelah selesai memindai semua tabel dalam database, koneksi socket ditutup dan direktori ditutup.
```
//-------Extras-------//

// Directory traversal
void listFilesRecursively(char *basePath, char *data)
{
    char path[1024];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            char ch, tb[1024];
            int i = 0;

            strcat(data, dp->d_name);
            strcat(data, "#");

            FILE *file = fopen(dp->d_name, "r");
            while ((ch = fgetc(file)) != EOF)
            {
                tb[i] = ch;
                i++;
            }
            fclose(file);
            tb[i] = '#';
            strcat(data, tb);

            // Construct new path from our base path
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);

            listFilesRecursively(path, data);
        }
    }

    closedir(dir);
}
```
Fungsi listFilesRecursively digunakan untuk melakukan traversal direktori secara rekursif, memindai setiap file dan subdirektori dari basePath. Setiap file yang ditemukan akan dibaca, kemudian isinya akan dimasukkan ke dalam string data dengan format tertentu yang menggunakan tanda # sebagai delimiter antara nama file dan isinya. Proses tersebut dilakukan secara rekursif untuk setiap subdirektori yang ditemukan dalam hirarki direktori yang diproses.
```
// DOWNLOAD DATABASE [database_name]
void downloadDB(int *socket, char db[])
{
    char path[1024], data[2048] = "";
    strcat(data, db);
    strcat(data, "#");

    struct dirent *dp;
    sprintf(path, "%s/%s", "databases", db);
    DIR *dir = opendir(db);

    if (!dir)
    {
        char message[1024] = "Failed to download databases!\n";
        send(*socket, message, strlen(message), 0);
        return;
    }
    listFilesRecursively(path, data);
    closedir(dir);
    send(*socket, data, strlen(data), 0);
}
```
Fungsi downloadDB bertujuan untuk mengunduh database dengan nama tertentu melalui socket yang ditentukan. Pertama, fungsi ini membangun jalur direktori ke database yang dimaksud. Kemudian, ia memeriksa keberadaan direktori tersebut. Jika direktori tidak ditemukan, fungsi akan mengirim pesan ke socket yang menyatakan bahwa pengunduhan database gagal. Jika direktori ditemukan, fungsi akan menggunakan listFilesRecursively untuk memindai semua file dan subdirektori dalam database secara rekursif. Isi dari file-file tersebut kemudian dikumpulkan ke dalam string data dengan format yang menggunakan # sebagai delimiter antara nama file dan isinya. Akhirnya, string data dikirim melalui socket untuk diunduh oleh penerima.<br>
<br>
**Main**
```
int main(int argc, char const *argv[])
{
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }
```
Program di atas membuat server socket, mendengarkan koneksi masuk, dan menerima koneksi dari klien. Server dikonfigurasi untuk menangani beberapa koneksi klien dengan memungkinkan antrian hingga 3 koneksi yang tertunda (listen(server_fd, 3)). Panggilan accept menunggu koneksi klien yang masuk dan, setelah diterima, membuat socket baru new_socket yang didedikasikan untuk koneksi klien tertentu. Socket ini akan digunakan untuk komunikasi antara server dan klien.
```
    // Users database: Users table and Permission Table
    if (access("databases/userDB/users.tb", F_OK) != 0)
    {
        createDB("userDB");
    }
    char *attribute[64];
    attribute[0] = "username";
    attribute[1] = "password";
    if (!tbExist("userDB", "users"))
    {
        // Data type: (string, string)
        char dt[2][32] = {"string", "string"};
        createTB("userDB", "users", attribute, 2, dt, 2);
    }
    attribute[0] = "database";
    attribute[1] = "user";
    if (!tbExist("userDB", "permission"))
    {
        // Data type: (string, string)
        char dt[2][32] = {"string", "string"};
        createTB("userDB", "permission", attribute, 2, dt, 2);
    }
```
Kode ini memeriksa keberadaan tabel users dan permission di basis data pengguna (userDB). Jika tabel-tabel ini tidak ada, itu akan membuatnya. Untuk tabel users, atribut username dan password dibuat dengan tipe data string. Untuk tabel permission, atribut database dan user dibuat dengan tipe data string. Hal ini memastikan bahwa struktur tabel yang diperlukan untuk penyimpanan informasi pengguna dan izin telah dibuat atau akan dibuat jika belum ada.
```
    // Loop command
    while (1)
    {
        // read variable
        char buffer[1024] = {0};
        char username[100], password[100];
        User current;

        // check user -> authentication
        valread = read(new_socket, buffer, 1024);
        if (strcmp(buffer, "root") == 0)
        {
            setUser(&current, buffer, buffer);
            char message[1024] = "Logged in as root!\n";
            send(new_socket, message, strlen(message), 0);
        }
        else if (strcmp(buffer, "error") == 0)
        {
            close(new_socket);
            return 0;
        }

        else
        {
            char username[30];
            char password[30];

            strcpy(username, buffer);
            memset(buffer, 0, sizeof(buffer));

            char message[1024] = "username received\n";
            send(new_socket, message, strlen(message), 0);

            valread = read(new_socket, buffer, 1024);
            strcpy(password, buffer);

            setUser(&current, username, password);
        }
        memset(buffer, 0, sizeof(buffer));

        if (!checkUser(&current))
        {
            char message[1024] = "authentication error";
            send(new_socket, message, strlen(message), 0);
            close(new_socket);
            return 0;
        }
```
Kode ini merupakan bagian dari sebuah loop yang menangani masukan dan autentikasi pengguna untuk perintah-perintah selanjutnya. Ketika ada koneksi baru, program akan membaca pesan yang diterima. Jika pesan adalah "root", itu akan mengatur pengguna saat ini sebagai root. Jika pesan adalah "error", program akan menutup soket dan keluar dari loop. Selain itu, program akan mengasumsikan bahwa pesan pertama yang diterima adalah username, kemudian meminta dan menerima password. Setelah mendapatkan username dan password, program memeriksa autentikasi dengan fungsi checkUser. Jika autentikasi gagal, pesan "authentication error" dikirim dan koneksi ditutup.
```
        // Current Database
        char currentDB[128];
        currentDB[0] = '\0';

        // All available operations from client
        while (1)
        {
            valread = read(new_socket, buffer, 1024);

            // exit program
            if (strcmp(buffer, "exit") == 0)
            {
                printf("Closing client\n");
                char message[1024] = "Program closed...\n";
                send(new_socket, message, strlen(message), 0);
                close(new_socket);
                return 0;
            }

            // Commands received from clients
            char com[128][64];
            int command_size = 0;
            splitCommands(buffer, com, &command_size);

            // 1st argument: CREATE
            if (strcmp(com[0], "CREATE") == 0)
            {
                // 2nd argument: DATABASE -> CREATE DATABASE
                if (strcmp(com[1], "DATABASE") == 0)
                {
                    // 3rd argument: [database_name] -> CREATE DATABASE [database_name]
                    if (strlen(com[2]) > 0)
                    {
                        // Database exist
                        if (dbExist(com[2]))
                        {
                            char message[1024] = "Error: Database already exists.\n";
                            send(new_socket, message, strlen(message), 0);
                        }
                        // Create database
                        else
                        {
                            createDB(com[2]);
                            grantPermission(com[2], current.username);
                            char message[1024] = "Database successfully created!\n";
                            send(new_socket, message, strlen(message), 0);
                            writeLog(&current, buffer);
                        }
                    }
                    else
                    {
                        char message[1024] = "Syntax Error: CREATE DATABASE [database_name]\n";
                        send(new_socket, message, strlen(message), 0);
                    }
                }

                // 2nd argument: TABLE -> CREATE TABLE
                else if (strcmp(com[1], "TABLE") == 0)
                {
                    // No database is selected yet
                    if (currentDB[0] == '\0')
                    {
                        char message[1024] = "No database selected yet, select one database: USE [database_name].\n";
                        send(new_socket, message, strlen(message), 0);
                    }
                    else
                    {
                        // 3rd to @command_size argument: [table_name] (attribute1 data_type, attribute2 data_type)
                        // -> CREATE TABLE [table_name] (attribute1 data_type, attribute2 data_type)
                        char *attribute[64], dt[128][32];
                        int i = 0, dt_size = 0, j = 0;
                        for (int j = 3; j < command_size; j += 2)
                        {
                            attribute[i++] = com[j];
                            strcpy(dt[dt_size++], com[j + 1]);
                        }

                        if (!tbExist(currentDB, com[2]))
                        {
                            createTB(currentDB, com[2], attribute, i, dt, dt_size);
                            writeLog(&current, buffer);
                            char message[1024] = "Table created\n";
                            send(new_socket, message, strlen(message), 0);
                        }
                        else
                        {
                            char message[1024] = "Error: Table already exist!\n";
                            send(new_socket, message, strlen(message), 0);
                        }
                    }
                }

                // 2nd argument: USER -> CREATE USER
                else if (strcmp(com[1], "USER") == 0)
                {
                    // Regular user is not allowed to create new user
                    if (!current.isRoot)
                    {
                        char message[1024] = "Permission denied!\n";
                        send(new_socket, message, strlen(message), 0);
                    }

                    // Current user is root
                    else if (current.isRoot)
                    {
                        // Syntax error
                        if (command_size != 6 || strcmp(com[3], "IDENTIFIED") != 0 || strcmp(com[4], "BY") != 0)
                        {
                            char message[1024] = "Syntax error: CREATE USER [username] IDENTIFIED BY [password]\n";
                            send(new_socket, message, strlen(message), 0);
                        }

                        // Create new user -> CREATE USER [username] IDENTIFIED BY [password]
                        else
                        {
                            createUser(com[2], com[5]);
                            char message[1024];
                            sprintf(message, "User %s has been created!\n", com[2]);
                            writeLog(&current, buffer);
                            send(new_socket, message, strlen(message), 0);
                        }
                    }
                }

                // 2nd argument: ERROR
                else
                {
                    char message[1024] = "Syntax Error: CREATE [DATABASE | TABLE | USER]\n";
                    send(new_socket, message, strlen(message), 0);
                }
            }
```
Kode ini merupakan loop yang menangani berbagai operasi dari klien. Saat menerima perintah, program melakukan beberapa pengecekan dan operasi berbeda berdasarkan perintah yang diberikan. Misalnya, saat menerima perintah "CREATE", program akan mengecek argumen selanjutnya. Jika argumen kedua adalah "DATABASE", program akan membuat database baru jika belum ada, dan memberikan izin kepada pengguna saat ini. Jika argumen kedua adalah "TABLE", program akan membuat tabel baru di database yang telah dipilih sebelumnya. Jika argumen kedua adalah "USER", program akan membuat pengguna baru jika pengguna saat ini adalah "root". Terdapat penanganan kesalahan sintaksis dan otorisasi yang sesuai dengan perintah yang diberikan.
```
            // 1st argument: USE
            else if (strcmp(com[0], "USE") == 0)
            {
                // 2nd argument: [database_name] -> USE [database_name]
                if (command_size != 2)
                {
                    char message[1024] = "Syntax error! Database name must be only 1 word: USE [database_name]\n";
                    send(new_socket, message, strlen(message), 0);
                }
                else if (dbExist(com[1]))
                {
                    // User database is not allowed to be change, even by root
                    if (strcmp(com[1], "userDB") == 0)
                    {
                        char message[1024] = "Permission denied!\n";
                        send(new_socket, message, strlen(message), 0);
                    }

                    else if (isAllowedToDB(current.username, com[1]))
                    {
                        strcpy(currentDB, com[1]);
                        writeLog(&current, buffer);
                        char message[1024];
                        sprintf(message, "%s database selected!\n", com[1]);
                        send(new_socket, message, strlen(message), 0);
                    }
                    else
                    {
                        char message[1024] = "Permission denied! You dont have access to that database!\n";
                        send(new_socket, message, strlen(message), 0);
                    }
                }
                else
                {
                    char message[1024] = "Error, database not found!\n";
                    send(new_socket, message, strlen(message), 0);
                }
            }
```
perintah "USE" yang bertujuan untuk mengganti database yang sedang aktif. Jika argumen perintah tidak sesuai dengan format yang diharapkan, program akan mengirim pesan kesalahan terkait sintaksis. Kemudian, program memeriksa apakah database yang ingin diakses tersedia. Jika ditemukan, akan dilakukan pemeriksaan izin akses terkait database tersebut untuk pengguna saat ini. Jika pengguna memiliki izin, database yang dipilih akan diatur sebagai database aktif untuk pengguna dan mencatat perubahan tersebut dalam log. Jika tidak, program akan memberikan pesan kesalahan yang menunjukkan bahwa pengguna tidak memiliki akses ke database tersebut.
```
            // 1st argument: INSERT
            else if (strcmp(com[0], "INSERT") == 0)
            {
                // 2nd argument: INTO -> INSERT INTO
                if (strcmp(com[1], "INTO") == 0)
                {
                    // No database is selected yet
                    if (currentDB[0] == '\0')
                    {
                        char message[1024] = "No database selected yet, select one database: USE [database_name].\n";
                        send(new_socket, message, strlen(message), 0);
                    }

                    // 3rd to @command_size argument: [table_name] (attribute1, attribute2, .......)
                    // -> INSERT INTO [table_name] (attribute1, attribute2, .......)
                    else if (command_size <= 3)
                    {
                        char message[1024] = "Syntax error: INSERT INTO [database_name] (attribute1, attribute2, .......)\n";
                        send(new_socket, message, strlen(message), 0);
                    }
                    else
                    {
                        char buffer[1024], data[1024], content[128][64], ch;
                        int i = 0, content_size = 0;
                        sprintf(buffer, "%s/%s/.%s_tb", "databases", currentDB, com[2]);
                        FILE *file = fopen(buffer, "r");

                        if (file)
                        {
                            while (fscanf(file, "%c", &ch) != EOF)
                            {
                                data[i] = ch;
                                i++;
                            }
                            data[i] = '\0';
                        }
                        splitCommands(data, content, &content_size);

                        // Remove 1-3rd argument: CREATE TABLE [table_name]
                        content_size -= 3;

                        // Count number of parameter
                        content_size /= 2;

                        // Deny command if number of parameter mismatch
                        if ((command_size - 3) < content_size)
                        {
                            char message[1024];
                            sprintf(message, "Too many arguments for table %s parameter!\n", com[2]);
                            send(new_socket, message, strlen(message), 0);
                        }
                        else if ((command_size - 3) > content_size)
                        {
                            char message[1024];
                            sprintf(message, "Too few arguments for table %s parameter!\n", com[2]);
                            send(new_socket, message, strlen(message), 0);
                        }

                        char *attribute[64];
                        i = 0;
                        for (int j = 3; j < command_size; j++)
                            attribute[i++] = com[j];

                        insertInto(currentDB, com[2], attribute, i);
                        writeLog(&current, buffer);

                        char message[1024] = "Data inserted, 1 row(s) affected\n";
                        send(new_socket, message, strlen(message), 0);
                    }
                }
                else
                {
                    char message[1024] = "Usage: INSERT INTO [database_name] (attribute1, attribute2, .......)\n";
                    send(new_socket, message, strlen(message), 0);
                }
            }
```
perintah "INSERT" yang bertujuan untuk memasukkan data ke dalam tabel tertentu di database yang aktif. Jika tidak ada database yang dipilih, program akan mengirim pesan kesalahan yang menunjukkan bahwa belum ada database yang dipilih. Setelah itu, program akan memeriksa apakah format perintah yang diterima sesuai dengan yang diharapkan, termasuk struktur tabel yang telah dibuat sebelumnya. Jika format tidak sesuai atau jumlah argumen tidak cocok dengan struktur tabel yang ada, program akan mengirim pesan kesalahan yang relevan. Jika semuanya sesuai, data akan dimasukkan ke dalam tabel yang ditargetkan dan informasi pencatatan akan disimpan dalam log.
```
            // 1st argument: SELECT
            else if (strcmp(com[0], "SELECT") == 0)
            {
                // No database is selected yet
                if (currentDB[0] == '\0')
                {
                    char message[1024] = "No database selected yet, select one database: USE [database_name].\n";
                    send(new_socket, message, strlen(message), 0);
                }
                else
                {
                    // 2nd argument: * -> SELECT *
                    if (strcmp(com[1], "*") == 0)
                    {
                        // 3rd & 4th argument: FROM [table_name] -> SELECT * FROM [table_name]
                        if (command_size >= 4)
                        {
                            if (command_size == 4)
                            {
                                selectAll(&new_socket, currentDB, com[3]);
                                writeLog(&current, buffer);
                            }

                            // 5th argument: WHERE -> SELECT * FROM [table_name] WHERE
                            else if (strcmp(com[4], "WHERE") == 0)
                            {
                                char col_name[64], value[64];

                                // 6th argument: [condition] -> SELECT * FROM [table_name] WHERE [column_name=value]
                                if (command_size == 6)
                                {
                                    int i = 0;
                                    // Get column name
                                    while (com[5][i] != '=')
                                    {
                                        col_name[i] = com[5][i];
                                        i++;
                                    }
                                    col_name[i] = '\0';

                                    i++;

                                    // Get value
                                    int j = 0;
                                    while (com[5][i] != '\0')
                                    {
                                        value[j] = com[5][i];
                                        i++;
                                        j++;
                                    }
                                    value[j] = '\0';
                                }

                                // 6th, 7th, and 8th argument: [condition] -> SELECT * FROM [table_name] WHERE [column_name = value]
                                else if (command_size == 8)
                                {
                                    strcpy(col_name, com[5]);
                                    strcpy(value, com[7]);
                                }

                                //
                                selectAllWhere(&new_socket, currentDB, com[3], col_name, value);
                                writeLog(&current, buffer);
                            }
                        }
                        // Syntax error
                        else
                        {
                            char message[1024] = "Syntax error: SELECT [column1, column2 | * ] FROM [table_name] (WHERE [column=value])\n";
                            send(new_socket, message, strlen(message), 0);
                        }
                    }

                    // 2nd argument: column 1, column2, ..... -> SELECT [column1, column2, .....]
                    else
                    {
                        // Selected columns
                        char cols[128][64];
                        int col_size = 0;

                        // Condition
                        char tb[64], value[64];

                        // Incremental value
                        int i = 1;
                        bool isValid = false, where = false;
                        while (i < command_size)
                        {
                            // All columns already selected, next argument: FROM -> SELECT [column1, column2, ......] FROM
                            if (strcmp(com[i], "FROM") == 0)
                            {
                                // Next argument: [table_name] -> SELECT [column1, column2, ......] FROM [table_name]
                                if (i + 1 < command_size)
                                {
                                    strcpy(tb, com[i + 1]);
                                    isValid = true;
                                }
                                break;
                            }

                            // Store all selected columns
                            else
                            {
                                strcpy(cols[col_size++], com[i]);
                                i++;
                            }
                        }

                        // Using "WHERE" argument -> SELECT [column1, column2, ......] FROM [table_name] WHERE
                        char condition_col[64];
                        if (strcmp(com[i + 2], "WHERE") == 0)
                        {
                            where = true;
                            int k = 0;

                            // SELECT [column1, column2, ......] FROM [table_name] WHERE [column_name=value]
                            if (command_size == i + 4)
                            {
                                // Get column name
                                while (com[i + 3][k] != '=')
                                {
                                    condition_col[k] = com[i + 3][k];
                                    k++;
                                }
                                condition_col[k] = '\0';

                                k++;

                                // Get value
                                int j = 0;

                                while (com[i + 3][k] != '\0')
                                {
                                    value[j] = com[i + 3][k];
                                    k++;
                                    j++;
                                }
                                value[j] = '\0';
                            }

                            // SELECT [column1, column2, ......] FROM [table_name] WHERE [column_name = value]
                            else if (command_size == i + 6)
                            {
                                strcpy(condition_col, com[i + 3]);
                                strcpy(value, com[i + 5]);
                            }
                        }

                        // Syntax error
                        if (!isValid)
                        {
                            char message[1024] = "Syntax error: SELECT [col1, col2, ... | * ] FROM [table_name] (WHERE [column=value])\n";
                            send(new_socket, message, strlen(message), 0);
                        }

                        // SELECT [column1, column2, ......] FROM [table_name]
                        else if (!where)
                        {
                            selectColFrom(&new_socket, currentDB, tb, cols, col_size);
                            writeLog(&current, buffer);
                        }

                        // SELECT [column1, column2, ......] FROM [table_name] WHERE [column=value]
                        if (where && isValid)
                        {
                            selectColFromWhere(&new_socket, currentDB, tb, cols, col_size, condition_col, value);
                            writeLog(&current, buffer);
                        }
                    }
                }
            }
```
perintah "SELECT" dalam bahasa SQL yang dapat digunakan untuk mengambil data dari tabel dalam database yang aktif. Jika tidak ada database yang dipilih, program akan mengirim pesan kesalahan. Program memeriksa struktur perintah "SELECT" yang diterima, seperti mengambil seluruh kolom dari tabel tertentu ("SELECT * FROM [table_name]") atau memilih kolom tertentu ("SELECT [column1, column2, ...] FROM [table_name]"). Program juga mengelola kondisi "WHERE" jika ada, yang memungkinkan pemfilteran hasil berdasarkan kriteria tertentu. Jika format perintah tidak sesuai atau kondisi tidak terpenuhi, program akan mengirim pesan kesalahan yang sesuai.
<br>
Adapun implementasi dari tiap kriteria tersebut dijelaskan di dalam kode. Misalnya, untuk perintah "SELECT * FROM [table_name]", fungsi selectAll dipanggil. Sedangkan untuk perintah "SELECT [column1, column2, ...] FROM [table_name]", fungsi selectColFrom digunakan. Kemudian, jika terdapat kondisi "WHERE" pada perintah, seperti "SELECT * FROM [table_name] WHERE [column_name=value]" atau "SELECT [column1, column2, ...] FROM [table_name] WHERE [column_name=value]", fungsi selectAllWhere atau selectColFromWhere dipanggil untuk memproses kondisi tersebut.
```
            // 1st argument: UPDATE
            else if (strcmp(com[0], "UPDATE") == 0)
            {
                // 2nd argument: [database_name] -> UPDATE [database_name]
                if (tbExist(currentDB, com[1]))
                {
                    // 3rd argument: SET -> UPDATE [database_name] SET
                    if (strcmp(com[2], "SET") == 0)
                    {
                        // UPDATE [database_name] SET [column = value]
                        if (command_size == 6 && strcmp(com[4], "=") == 0)
                        {
                            int affected = updateSet(currentDB, com[1], com[3], com[5]);
                            char buffer[128];
                            sprintf(buffer, "Database updated, %d row(s) affected.\n", affected);
                            send(new_socket, buffer, strlen(buffer), 0);
                            writeLog(&current, buffer);
                        }

                        // UPDATE [database_name] SET [column = value] WHERE [column = value]
                        else if (command_size == 10 && strcmp(com[8], "=") == 0)
                        {
                            int affected = updateSetWhere(currentDB, com[1], com[3], com[5], com[9], com[7]);
                            char buffer[128];
                            sprintf(buffer, "Database updated, %d row(s) affected.\n", affected);
                            send(new_socket, buffer, strlen(buffer), 0);
                            writeLog(&current, buffer);
                        }
                        else
                        {
                            char message[1024] = "Syntax error: UPDATE [table_name] SET [column = value]\n";
                            send(new_socket, message, strlen(message), 0);
                        }
                    }
                    else
                        send(new_socket, "Syntax error: UPDATE [table_name] SET [column = value]\n",
                             strlen("Syntax error: UPDATE [table_name] SET [column = value]\n"), 0);
                }
                else
                    send(new_socket, "Table not found!\n", strlen("Table not found!\n"), 0);
            }
```
perintah SQL "UPDATE" untuk memperbarui data dalam tabel yang dipilih. Program memeriksa format perintah yang diterima, memvalidasi tabel yang dituju, dan mengeksekusi pembaruan data menggunakan fungsi updateSet atau updateSetWhere tergantung pada struktur perintah, dengan mengirim pesan kesalahan jika format tidak sesuai dengan yang diharapkan.
```
            // 1st argument: DELETE
            else if (strcmp(com[0], "DELETE") == 0)
            {
                // 2nd argument: FROM -> DELETE FROM
                if (strcmp(com[1], "FROM") == 0)
                {
                    // No database is selected yet
                    if (currentDB[0] == '\0')
                        send(new_socket, "No database selected yet, select one database: USE [database_name].\n",
                             strlen("No database selected yet, select one database: USE [database_name].\n"), 0);

                    // 3rd argument: [table_name] -> DELETE FROM [table_name]
                    else
                    {
                        if (tbExist(currentDB, com[2]))
                        {
                            if (command_size == 7 && strcmp(com[3], "WHERE") == 0 && strcmp(com[5], "=") == 0)
                            {
                                // Delete with WHERE
                                int affected = deleteFrom(currentDB, com[2], com[4], com[6]);
                                char buffer[128];
                                sprintf(buffer, "%d row(s) affected.\n", affected);
                                send(new_socket, buffer, strlen(buffer), 0);
                            }
                            else
                            {
                                // Delete without where
                                int affected = deleteFrom(currentDB, com[2], "#", "#");
                                char buffer[128];
                                sprintf(buffer, "%d row(s) affected.\n", affected);
                                send(new_socket, buffer, strlen(buffer), 0);
                            }
                            writeLog(&current, buffer);
                        }
                        else
                            send(new_socket, "Table not found!\n", strlen("Table not found!\n"), 0);
                    }
                }
                // Syntax error
                else
                    send(new_socket, "Syntax error: DELETE FROM [table_name]\n", strlen("Syntax error: DELETE FROM [table name]\n"), 0);
            }
```
SQL "DELETE" untuk menghapus entri dari tabel yang dipilih. Program memeriksa apakah sudah ada database yang dipilih, memvalidasi tabel yang dituju, dan menjalankan perintah penghapusan entri. Jika perintah berisi klausa WHERE, program memperhatikan kondisi tersebut dan hanya menghapus entri yang memenuhi syarat. Jika tidak, program akan menghapus semua entri dalam tabel tersebut. Pesan kesalahan dikirim jika format perintah tidak sesuai dengan yang diharapkan.
```
            // 1st argument: GRANT
            else if (strcmp(com[0], "GRANT") == 0)
            {
                // Syntax error
                if (command_size != 5 || strcmp(com[1], "PERMISSION") != 0 || strcmp(com[3], "INTO") != 0)
                    send(new_socket, "Syntax Error: GRANT PERMISSION [database_name] INTO [username]\n",
                         strlen("Syntax Error: GRANT PERMISSION [database_name] INTO [username]\n"), 0);

                // 2nd, 3rd, 4th, 5th argument: PERMISSION [database_name] INTO [username]
                // -> GRANT PERMISSION [database_name] INTO [username]
                else
                {
                    // Regular user don't have permission to grant access for another users to databases
                    if (!current.isRoot)
                        send(new_socket, "Permission denied!\n", strlen("Permission denied!\n"), 0);

                    else if (current.isRoot)
                    {
                        grantPermission(com[2], com[4]);
                        char message[1024];
                        sprintf(message, "%s now has permission to database %s\n", com[4], com[2]);
                        send(new_socket, message, strlen(message), 0);
                        writeLog(&current, buffer);
                    }
                }
            }
```
SQL "GRANT" untuk memberikan izin akses ke database kepada pengguna tertentu. Program memeriksa sintaksis perintah dan kemudian memverifikasi izin yang diberikan kepada pengguna saat ini. Jika pengguna yang saat ini masuk adalah pengguna root, program akan memberikan izin tersebut kepada pengguna yang dituju ke database yang ditentukan. Jika bukan, program akan mengirimkan pesan penolakan izin. Setelah mengeluarkan izin, program akan mencatat kegiatan tersebut ke dalam log.
```
            // 1st argument: DROP
            else if (strcmp(com[0], "DROP") == 0)
            {
                if (command_size > 2)
                {
                    // 2nd argument: DATABASE -> DROP DATABASE
                    if (strcmp(com[1], "DATABASE") == 0)
                    {
                        // 3rd argument: [database_name] -> DROP DATABASE [database_name]
                        if (dbExist(com[2]))
                        {
                            if (isAllowedToDB(current.username, com[2]))
                            {
                                dropDB(com[2]);
                                writeLog(&current, buffer);
                                char message[1024];
                                sprintf(message, "%s database dropped.\n", com[2]);
                                send(new_socket, message, strlen(message), 0);
                            }
                            else
                                send(new_socket, "Permission denied!\n", strlen("Permission denied!\n"), 0);
                        }
                        else
                            send(new_socket, "Database not found!\n", strlen("Database not found!\n"), 0);
                    }

                    // 2nd argument: TABLE -> DROP TABLE
                    else if (strcmp(com[1], "TABLE") == 0)
                    {
                        // 3rd argument: [table_name] -> DROP TABLE [table_name]
                        if (tbExist(currentDB, com[2]))
                        {
                            dropTB(&new_socket, currentDB, com[2]);
                            writeLog(&current, buffer);
                        }
                        else
                            send(new_socket, "Table not found!\n", strlen("Table not found!\n"), 0);
                    }

                    // 2nd argument: COLUMN -> DROP COLUMN
                    else if (strcmp(com[1], "COLUMN") == 0)
                    {
                        // Syntax error
                        if (command_size != 5)
                            send(new_socket, "Syntax Error: DROP COLUMN [column_name] FROM [table_name]\n",
                                 strlen("Syntax Error: DROP COLUMN [column_name] FROM [table_name]\n"), 0);

                        // 3rd, 4th, and 5th argument: [column] FROM [table_name]
                        // -> DROP COLUMN [column] FROM [table_name]
                        else
                        {
                            dropColumn(currentDB, com[4], com[2], "");
                            writeLog(&current, buffer);
                        }
                    }
                }
                // Syntax error
                else
                {
                    char message[1024] = "Syntax Error: DROP [DATABASE | TABLE | COLUMN]\n";
                    send(new_socket, message, strlen(message), 0);
                }
            }
```
"DROP" untuk menghapus database, tabel, atau kolom dari tabel. Program memeriksa argumen perintah untuk mengidentifikasi apa yang perlu dihapus (database, tabel, atau kolom). Jika perintah adalah "DROP DATABASE", program memeriksa apakah database yang ditentukan ada, dan jika pengguna saat ini memiliki izin untuk menghapusnya, maka database tersebut dihapus. Jika perintah adalah "DROP TABLE", program memeriksa keberadaan tabel dalam database saat ini dan menghapusnya jika ada. Jika perintah adalah "DROP COLUMN", program menghapus kolom dari tabel yang ditentukan jika syntax sesuai. Jika ada kesalahan dalam sintaks, program mengirim pesan kesalahan yang sesuai. Setelah operasi penghapusan, kegiatan tersebut dicatat dalam log.
```
            // 1st and 2nd argument: BACKUP DATABASE
            else if (command_size == 3 && strcmp(com[0], "BACKUP") == 0 && strcmp(com[1], "DATABASE") == 0)
            {
                // 3rd argument: [database_name] -> BACKUP DATABASE [database_name]
                if (dbExist(com[2]))
                {
                    scanDB(&new_socket, com[2]);
                    writeLog(&current, buffer);
                }

                else
                {
                    char message[1024] = "Database not found!\n";
                    send(new_socket, message, strlen(message), 0);
                }
            }
```
SQL "BACKUP DATABASE" yang digunakan untuk mencadangkan seluruh database. Program memeriksa argumen perintah untuk memastikan bahwa perintah adalah "BACKUP DATABASE" dan memeriksa apakah database yang ditentukan ada. Jika database ditemukan, program memulai proses pemindaian dan pencadangan dengan memanggil fungsi scanDB. Setelah pencadangan selesai, kegiatan tersebut dicatat dalam log pengguna. Jika database tidak ditemukan, program mengirimkan pesan kesalahan yang sesuai.
```
            // 1st and 2nd argument: DOWNLOAD DATABASE
            else if (command_size == 3 && strcmp(com[0], "DOWNLOAD") == 0 && strcmp(com[1], "DATABASE") == 0)
            {
                // 3rd argument: [database_name] -> DOWNLOAD DATABASE [database_name]
                if (dbExist(com[2]))
                {
                    downloadDB(&new_socket, com[2]);
                    writeLog(&current, buffer);
                }

                else
                {
                    char message[1024] = "Database not found!\n";
                    send(new_socket, message, strlen(message), 0);
                }
            }
```
"DOWNLOAD DATABASE", yang memungkinkan pengguna untuk mengunduh seluruh database tertentu. Program memeriksa argumen perintah untuk memastikan bahwa perintah adalah "DOWNLOAD DATABASE" dan memeriksa keberadaan database yang ditentukan. Jika database ditemukan, program memulai proses pengunduhan dengan memanggil fungsi downloadDB. Setelah pengunduhan selesai, kegiatan tersebut dicatat dalam log pengguna. Jika database tidak ditemukan, program mengirimkan pesan kesalahan yang sesuai.
```
            // Invalid command
            else
            {
                char message[1024] = "Invalid command!\n";
                send(new_socket, message, strlen(message), 0);
            }

            memset(buffer, 0, sizeof(buffer));
            memset(com, 0, sizeof(com));
        }
    }

    return 0;
}
```
bagian dari server yang menjalankan sistem manajemen basis data (DBMS) untuk menerima, memproses, dan menanggapi perintah dari klien. Melalui loop utama, program ini mengidentifikasi perintah yang diterima dari klien dan menjalankan fungsi-fungsi yang sesuai seperti pembuatan basis data, tabel, penghapusan, pengambilan, penyisipan data, serta manajemen izin dan backup database. Keseluruhan program berfungsi sebagai perantara yang mengatur akses terhadap basis data yang terkelola berdasarkan perintah yang diterima dari pengguna.

## cron.sh
```
0 * * * * cd /home/syl/sisop/FP/database && ./program_dump_database -u root -p root > database_name.database && zip -rm database_name.database
```
cd /home/syl/sisop/FP/database: Mengubah direktori kerja ke /home/syl/sisop/FP/database.<br>
./program_dump_database -u root -p root > database_name.database: Menjalankan program dengan nama program_dump_database dengan opsi -u root -p root (kemungkinan username dan password), dan output dari perintah tersebut disimpan dalam file bernama database_name.database.<br>
zip -rm database_name.database: Membuat arsip zip dari file database_name.database dalam direktori saat ini.<br>

## client_dump.c
```
#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <ctype.h>
#include <pthread.h>
#include <dirent.h>
#include <fcntl.h>
#define PORT 8080

int sock = 0, valread;
int status = 0;
pthread_t thread;

// Check if user is root
bool isRoot()
{
    return (getuid() == 0);
}

bool authenticate(int argc, char const *argv[])
{
    char buffer[1024] = {0};
    if (isRoot())
        send(sock, "root", sizeof("root"), 0);
    else if (argc != 6 || strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0)
    {
        send(sock, "error", strlen("error"), 0);
        printf("Authentication error!\n");
        printf("Syntax to start the client:\n");
        printf("./program -u [username] -p [password]\n");
        return false;
    }
    else
    {
        send(sock, argv[2], strlen(argv[2]), 0);
        valread = read(sock, buffer, 1024);
        send(sock, argv[4], strlen(argv[4]), 0);
    }
    memset(buffer, 0, sizeof(buffer));
    return true;
}

void *messageHandling(void *arg)
{
    char buffer[1024];
    memset(buffer, 0, sizeof(buffer));
    while (read(*(int *)arg, buffer, 1024) != 0)
    {
        if (strlen(buffer))
            printf("%s", buffer);
        memset(buffer, 0, sizeof(buffer));
    }
}

int main(int argc, char const *argv[])
{
    struct sockaddr_in address,
        serv_addr;
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("failed to create socket\n");
        return -1;
    }
    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
        printf("Invalid address\n");
        return -1;
    }
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("Connection failed\n");
        return -1;
    }
    if (!authenticate(argc, argv))
        return 0;
    char buffer[1024];
    // Command for dump: BACKUP DATABASE [database_name]
    strcpy(buffer, "BACKUP DATABASE ");
    strcat(buffer, argv[5]);
    strcat(buffer, ";");
    pthread_t dataHandler;
    pthread_create(&dataHandler, NULL, &messageHandling, &sock);
    send(sock, buffer, strlen(buffer), 0);
    memset(buffer, 0, sizeof(buffer));
    // Join thread
    pthread_join(dataHandler, NULL);
    return 0;
}
```
**Penjelasan Kode**
```
#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <ctype.h>
#include <pthread.h>
#include <dirent.h>
#include <fcntl.h>

#define PORT 8080
```
- Program menggunakan beberapa header file untuk berbagai operasi, termasuk operasi I/O, soket, thread, dan sistem file.
- #define PORT 8080 mendefinisikan nomor port yang akan digunakan untuk koneksi.

```
int sock = 0, valread;
int status = 0;
pthread_t thread;
```
- sock: Variabel untuk menyimpan file descriptor soket.
- valread: Variabel untuk menyimpan nilai kembalian dari fungsi read.
- status: Status yang belum digunakan.
- pthread_t thread: Variabel untuk menyimpan identifikasi thread.

```
bool isRoot()
{
    return (getuid() == 0);
}
```
- Fungsi ini mengembalikan true jika pengguna yang menjalankan program memiliki hak akses root (UID 0), dan false jika tidak.

```
bool authenticate(int argc, char const *argv[])
{
    char buffer[1024] = {0};

    if (isRoot())
        send(sock, "root", sizeof("root"), 0);
    else if (argc != 6 || strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0)
    {
        send(sock, "error", strlen("error"), 0);
        printf("Authentication error!\n");
        printf("Syntax to start the client:\n");
        printf("./program -u [username] -p [password]\n");
        return false;
    }
    else
    {
        send(sock, argv[2], strlen(argv[2]), 0);
        valread = read(sock, buffer, 1024);
        send(sock, argv[4], strlen(argv[4]), 0);
    }
    memset(buffer, 0, sizeof(buffer));
    return true;
}
```
- Fungsi ini menangani proses otentikasi pengguna.
- Jika pengguna adalah root, maka program mengirim pesan "root" ke server.
- Jika bukan root, maka fungsi memeriksa jumlah dan format argumen yang diberikan pada saat menjalankan program. Jika sesuai, ia mengirimkan nama pengguna dan kata sandi ke server; jika tidak, ia mengirim pesan kesalahan ke server dan mencetak pesan kesalahan.

```
void *messageHandling(void *arg)
{
    char buffer[1024];
    memset(buffer, 0, sizeof(buffer));

    while (read(*(int *)arg, buffer, 1024) != 0)
    {
        if (strlen(buffer))
            printf("%s", buffer);
        memset(buffer, 0, sizeof(buffer));
    }
}
```
- Fungsi ini dijalankan dalam thread terpisah dan bertanggung jawab untuk menangani pesan yang diterima dari server.
- Menggunakan read untuk membaca pesan dari soket dan menampilkan pesan tersebut ke layar.

```
int main(int argc, char const *argv[])
{
    struct sockaddr_in address,
        serv_addr;
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("failed to create socket\n");
        return -1;
    }
    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
        printf("Invalid address\n");
        return -1;
    }
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("Connection failed\n");
        return -1;
    }
    if (!authenticate(argc, argv))
        return 0;
    char buffer[1024];
    // Command for dump: BACKUP DATABASE [database_name]
    strcpy(buffer, "BACKUP DATABASE ");
    strcat(buffer, argv[5]);
    strcat(buffer, ";");
    pthread_t dataHandler;
    pthread_create(&dataHandler, NULL, &messageHandling, &sock);
    send(sock, buffer, strlen(buffer), 0);
    memset(buffer, 0, sizeof(buffer));
    // Join thread
    pthread_join(dataHandler, NULL);
    return 0;
}
```
- Membuat soket dan menghubungkan ke server menggunakan protokol TCP/IP.
- Memanggil fungsi authenticate untuk melakukan otentikasi pengguna.
- Membuat dan menjalankan thread untuk menangani pesan yang diterima dari server.
- Mengirimkan perintah SQL ke server untuk melakukan backup database.
- Menunggu hingga thread yang menangani pesan selesai dijalankan sebelum program berakhir.

## client.c
```
// check if user is root
bool isRoot()
{
    return (getuid() == 0);
}
```
Fungsi ini mengembalikan 'true' jika program dijalankan oleh pengguna dengan hak akses root (UID 0).

```
// authentication from client side
bool authenticaticate(int argc, char const *argv[], int *sock)
{
    char buffer[1024] = {0};
    int valread;

    if (isRoot())
    {
        send(*sock, "root", sizeof("root"), 0);
        valread = read(*sock, buffer, 1024);
        printf("%s", buffer);
    }
    else if (argc != 5 || strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0)
    {
        send(*sock, "error", strlen("error"), 0);
        printf("Authentication error!\n");
        printf("Syntax to start the client:\n");
        printf("./program -u [username] -p [password]\n");
        return false;
    }
    else
    {
        send(*sock, argv[2], strlen(argv[2]), 0);
        valread = read(*sock, buffer, 1024);
        printf("%s", buffer);
        send(*sock, argv[4], strlen(argv[4]), 0);
    }
    memset(buffer, 0, sizeof(buffer));

    return true;
}
```
Fungsi authenticate() adalah fungsi yang bertanggung jawab untuk menangani autentikasi pada sisi klien (client). Fungsi ini memiliki tugas untuk memeriksa apakah program dijalankan sebagai root atau jika tidak, memeriksa argumen baris perintah untuk username dan password, kemudian mengirim informasi autentikasi ke server.
- Pertama, fungsi memeriksa apakah program dijalankan sebagai root dengan memanggil fungsi isRoot(). Jika ya, maka program mengirimkan pesan "root" ke server dan membaca respons dari server.
- Jika tidak dijalankan sebagai root, maka fungsi memeriksa apakah jumlah argumen dan format argumen baris perintah sesuai dengan yang diharapkan untuk autentikasi. Jika tidak sesuai, program mengirim pesan "error" ke server. fungsi mengirimkan username (dari argumen baris perintah) ke server, membaca respons dari server, kemudian mengirimkan password (juga dari argumen baris perintah) ke server.
Fungsi mengembalikan true jika autentikasi berhasil, dan false jika terdapat kesalahan autentikasi.


Fungsi main() merupakan fungsi utama dalam program klien (client) yang melakukan inisialisasi, membuat koneksi ke server, menangani autentikasi, dan mengatur proses pengiriman dan penerimaan data antara klien dan server.
```
struct sockaddr_in address;
int sock = 0, valread;
struct sockaddr_in serv_addr;
char buffer[1024] = {0};
```
Deklarasi variabel-variabel yang diperlukan untuk socket dan buffer.

```
if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
{
    printf("\n Socket creation error\n");
    return -1;
}
```
Membuat socket menggunakan fungsi socket(). Jika gagal, program mencetak pesan kesalahan dan keluar.

```
memset(&serv_addr, '0', sizeof(serv_addr));
serv_addr.sin_family = AF_INET;
serv_addr.sin_port = htons(PORT);
if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
{
    printf("\nInvalid address/Address not supported\n");
    return -1;
}
```
Inisialisasi struktur alamat server dengan alamat IP "127.0.0.1" (localhost) dan port yang telah ditentukan.

```
if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
{
    printf("\nConnection Failed\n");
    return -1;
}
```
Menghubungkan ke server menggunakan fungsi connect(). Jika gagal, program mencetak pesan kesalahan dan keluar.

```
if (!authenticate(argc, argv, &sock))
{
    return 0;
}
```
Memanggil fungsi autentikasi (authenticate()) untuk menangani proses autentikasi.

```
while (1)
    {
        scanf("%[^\n]s", buffer);
        getchar();
        send(sock, buffer, strlen(buffer), 0);

        if (strcmp(buffer, "exit") == 0)
        {
            printf("Client closed\n");
            return 0;
        }

        valread = read(sock, received, sizeof(received));

        if (strcmp(strtok(buffer, " "), "DOWNLOAD") && strcmp(received, "Failed to download databases!\n") != 0)
        {
            char *db, *tb;
            db = strtok(received, "#");

            mkdir(db, 0777);
            while (true)
            {
                char buffer[1024];
                tb = strtok(NULL, "#");

                if (tb == NULL)
                    break;

                sprintf(buffer, "%s/%s", db, tb);
                FILE *file = fopen(buffer, "w");
                if (file)
                {
                    tb = strtok(NULL, "#");
                    fprintf(file, "%s", tb);
                }
                fclose(file);
            }
        }

        else
            printf("%s\n", received);

        memset(received, 0, sizeof(received));
        memset(buffer, 0, sizeof(buffer));
    }
```
Program memasuki loop tak terbatas untuk mengirim dan menerima data dari server. Jika perintah yang dikirim adalah "exit", program akan keluar dari loop dan menutup koneksi.

```
if (strcmp(strtok(buffer, " "), "DOWNLOAD") && strcmp(received, "Failed to download databases!\n") != 0)
        {
            char *db, *tb;
            db = strtok(received, "#");

            mkdir(db, 0777);
            while (true)
            {
                char buffer[1024];
                tb = strtok(NULL, "#");

                if (tb == NULL)
                    break;

                sprintf(buffer, "%s/%s", db, tb);
                FILE *file = fopen(buffer, "w");
                if (file)
                {
                    tb = strtok(NULL, "#");
                    fprintf(file, "%s", tb);
                }
                fclose(file);
            }
        }
```
Jika perintah adalah "DOWNLOAD" dan operasi unduh berhasil, program akan membuat direktori dan menyimpan file-file yang diterima.

## SCREENSHOT
- kode database.c
![Database C](https://gitlab.com/sylxer/sisop-praktikum-fp-2023-mh-it13/-/raw/main/Screenshot/database_c.png)

- kode client.c
![Client C](https://gitlab.com/sylxer/sisop-praktikum-fp-2023-mh-it13/-/raw/main/Screenshot/client_c.png)

- kode client_dump.c
![client_dump C](https://gitlab.com/sylxer/sisop-praktikum-fp-2023-mh-it13/-/raw/main/Screenshot/client_dump.png)

- Log
![Log](https://gitlab.com/sylxer/sisop-praktikum-fp-2023-mh-it13/-/raw/main/Screenshot/log.png)

- Menjalankan Database
![Run Database](https://gitlab.com/sylxer/sisop-praktikum-fp-2023-mh-it13/-/raw/main/Screenshot/run_database.png)

- Mengakses Root
![Akses Root](https://gitlab.com/sylxer/sisop-praktikum-fp-2023-mh-it13/-/raw/main/Screenshot/root.png)

- Membuat User Baru
![User Baru](https://gitlab.com/sylxer/sisop-praktikum-fp-2023-mh-it13/-/raw/main/Screenshot/create_user.png)

- Membuat Database
![New Database](https://gitlab.com/sylxer/sisop-praktikum-fp-2023-mh-it13/-/raw/main/Screenshot/create_database.png)

- Memberi Hak Akses
![Hak Akses](https://gitlab.com/sylxer/sisop-praktikum-fp-2023-mh-it13/-/raw/main/Screenshot/grant_permission.png)

- Run Program dengan User
![User Run](https://gitlab.com/sylxer/sisop-praktikum-fp-2023-mh-it13/-/raw/main/Screenshot/user_run.png)

- Membuat Tabel
![Membuat Tabel](https://gitlab.com/sylxer/sisop-praktikum-fp-2023-mh-it13/-/raw/main/Screenshot/create_table.png)

- Menghapus Database
![Drop Database](https://gitlab.com/sylxer/sisop-praktikum-fp-2023-mh-it13/-/raw/main/Screenshot/drop_database.png)

- Mengisi Kolom 
![Insert Table](https://gitlab.com/sylxer/sisop-praktikum-fp-2023-mh-it13/-/raw/main/Screenshot/insert_tabel.png)

- Menghapus Tabel  
![Delete Table](https://gitlab.com/sylxer/sisop-praktikum-fp-2023-mh-it13/-/raw/main/Screenshot/delete_table.png)

- Delete From
![Delete From](https://gitlab.com/sylxer/sisop-praktikum-fp-2023-mh-it13/-/raw/main/Screenshot/delete_from.png)

- List User yang Terdaftar
![List User](https://gitlab.com/sylxer/sisop-praktikum-fp-2023-mh-it13/-/raw/main/Screenshot/list_user.png)

- List Hak Akses 
![List Permission](https://gitlab.com/sylxer/sisop-praktikum-fp-2023-mh-it13/-/raw/main/Screenshot/list_permission.png)

