#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <ctype.h>
#include <pthread.h>
#include <dirent.h>
#define PORT 8080

// check if user is root
bool isRoot()
{
    return (getuid() == 0);
}

// authentication from client side
bool authenticaticate(int argc, char const *argv[], int *sock)
{
    char buffer[1024] = {0};
    int valread;

    if (isRoot())
    {
        send(*sock, "root", sizeof("root"), 0);
        valread = read(*sock, buffer, 1024);
        printf("%s", buffer);
    }
    else if (argc != 5 || strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0)
    {
        send(*sock, "error", strlen("error"), 0);
        printf("Authentication error!\n");
        printf("Syntax to start the client:\n");
        printf("./program -u [username] -p [password]\n");
        return false;
    }
    else
    {
        send(*sock, argv[2], strlen(argv[2]), 0);
        valread = read(*sock, buffer, 1024);
        printf("%s", buffer);
        send(*sock, argv[4], strlen(argv[4]), 0);
    }
    memset(buffer, 0, sizeof(buffer));

    return true;
}

int main(int argc, char const *argv[])
{
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0};

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error\n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
        printf("\nInvalid address/Address not supported\n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed\n");
        return -1;
    }

    if (!authenticaticate(argc, argv, &sock))
    {
        return 0;
    }

    char received[1024];

    while (1)
    {
        scanf("%[^\n]s", buffer);
        getchar();
        send(sock, buffer, strlen(buffer), 0);

        if (strcmp(buffer, "exit") == 0)
        {
            printf("Client closed\n");
            return 0;
        }

        valread = read(sock, received, sizeof(received));

        if (strcmp(strtok(buffer, " "), "DOWNLOAD") && strcmp(received, "Failed to download databases!\n") != 0)
        {
            char *db, *tb;
            db = strtok(received, "#");

            mkdir(db, 0777);
            while (true)
            {
                char buffer[1024];
                tb = strtok(NULL, "#");

                if (tb == NULL)
                    break;

                sprintf(buffer, "%s/%s", db, tb);
                FILE *file = fopen(buffer, "w");
                if (file)
                {
                    tb = strtok(NULL, "#");
                    fprintf(file, "%s", tb);
                }
                fclose(file);
            }
        }

        else
            printf("%s\n", received);

        memset(received, 0, sizeof(received));
        memset(buffer, 0, sizeof(buffer));
    }

    return 0;
}
