#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <ctype.h>
#include <pthread.h>
#include <dirent.h>
#include <time.h>
#define PORT 8080

pthread_t tid[3000];

typedef struct user_t
{
    char username[30];
    char password[30];
    bool isRoot;
} User;

//-------Database and Tables checker-------//

// Check if database exist
bool dbExist(char name[])
{
    char buffer[256];
    sprintf(buffer, "%s/%s", "databases", name);
    DIR *dir = opendir(buffer);
    if (dir)
    {
        closedir(dir);
        return true;
    }
    return false;
}

// Check if table exist
bool tbExist(char *db, char *tb)
{
    char path[256];
    sprintf(path, "%s/%s/%s.tb", "databases", db, tb);
    FILE *file = fopen(path, "r");
    if (file)
    {
        fclose(file);
        return true;
    }
    return false;
}

// Check if string is in column
bool isInCol(char str[64], const char arr[128][64], int arr_size)
{
    for (int i = 0; i < arr_size; i++)
    {
        if (strcmp(str, arr[i]) == 0)
            return 1;
    }
    return false;
}

//-------Check commands from client-------//

// Check if char is alphanumeric
bool alphanum(char c)
{
    if (c >= 'A' && c <= 'Z')
        return true;
    else if (c >= 'a' && c <= 'z')
        return true;
    else if (c >= '0' && c <= '9')
        return true;
    else if (c == '*' || c == '=')
        return true;
    return false;
}

// Split commands given from client
void splitCommands(const char *buffer, char com[128][64], int *size)
{
    int i = 0, j = 0, k = 0;
    while (i < strlen(buffer))
    {
        if (!alphanum(buffer[i]))
        {
            if (k)
            {
                com[j][k++] = '\0';
                j++;
                k = 0;
            }
        }
        else
            com[j][k++] = buffer[i];

        i++;
    }
    if (k)
    {
        com[j][k++] = '\0';
        j++;
        k = 0;
    }
    *size = j;
}

//-------Data Deifinition Language-------//

// CREATE DATABASE
void createDB(char *db)
{
    mkdir("databases", 0777);
    char buffer[256];
    sprintf(buffer, "%s/%s", "databases", db);
    mkdir(buffer, 0777);
}

// CREATE TABLE
void createTB(char *db, char *tb, char *attribute[64], int size, char dt[128][32], int dt_size)
{
    char buffer[256];
    sprintf(buffer, "%s/%s/%s.tb", "databases", db, tb);

    FILE *file = fopen(buffer, "w");

    // File not found
    if (!file)
        return;

    for (int i = 0; i < size; i++)
    {
        fprintf(file, "%s,", attribute[i]);
    }
    fclose(file);

    // Create table information
    sprintf(buffer, "%s/%s/.%s_tb", "databases", db, tb);
    file = fopen(buffer, "w");
    if (file)
    {
        fprintf(file, "CREATE TABLE %s (", tb);
        for (int i = 0; i < dt_size; i++)
        {
            fprintf(file, "%s %s", attribute[i], dt[i]);
            if (i != dt_size - 1)
                fprintf(file, ", ");
        }

        fprintf(file, ");");
        fclose(file);
    }
}

//-------Data Manipulation Language-------//
void insertInto(char *db, char *tb, char *data[64], int size)
{
    char buffer[256];
    sprintf(buffer, "%s/%s/%s.tb", "databases", db, tb);
    FILE *file = fopen(buffer, "a");

    // File not exist
    if (!file)
        return;

    fprintf(file, "\n");
    for (int i = 0; i < size; i++)
    {
        fprintf(file, "%s,", data[i]);
    }
    fclose(file);
}

// SELECT * FROM [table_name]
void selectAll(int *socket, const char *db, const char *tb)
{
    char buffer[256];
    sprintf(buffer, "%s/%s/%s.tb", "databases", db, tb);
    FILE *file = fopen(buffer, "r");
    if (!file)
        return;

    char data[256], ch;
    int data_size = 0;

    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == '\n')
        {
            if (data_size)
            {
                data[data_size++] = '\n';
                data[data_size++] = '\0';
                send(*socket, data, strlen(data), 0);
                data_size = 0;
            }
        }
        else
            data[data_size++] = ch;
    }
    if (data_size)
    {
        data[data_size++] = '\n';
        data[data_size++] = '\0';
        send(*socket, data, strlen(data), 0);
        data_size = 0;
    }
    fclose(file);
}

// SELECT * FROM [table_name] WHERE [condition]
void selectAllWhere(int *socket, const char *db, const char *tb, const char *col, const char *value)
{
    char buffer[256];
    sprintf(buffer, "%s/%s/%s.tb", "databases", db, tb);
    FILE *file = fopen(buffer, "r");
    if (!file)
        return;

    char ch, data[256], col_i[256];
    int data_size = 0, col_size_i = 0, col_number = 1, col_num_i = 1;
    bool isInHeader = true, colFound = false, row_valid = false;

    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == '\n')
        {
            col_num_i = 1;

            if (data_size)
            {
                data[data_size++] = '\n';
                data[data_size++] = '\0';
                if (row_valid || isInHeader)
                    send(*socket, data, strlen(data), 0);
                data_size = 0;
            }
            isInHeader = false;
            row_valid = false;
        }
        else
        {
            if (ch == ',')
            {
                col_i[col_size_i] = '\0';
                col_size_i = 0;

                if (col_num_i == col_number)
                {
                    if (!isInHeader && colFound)
                    {
                        if (strcmp(value, col_i) == 0)
                            row_valid = true;
                    }
                }
                col_num_i++;

                if (isInHeader && !colFound)
                {
                    if (strcmp(col, col_i) == 0)
                        colFound = true;
                    else
                        col_number++;
                }
                memset(col_i, 0, sizeof(col_i));
            }
            else
                col_i[col_size_i++] = ch;

            data[data_size++] = ch;
        }
    }

    if (data_size)
    {
        data[data_size++] = '\n';
        data[data_size++] = '\0';
        if (row_valid)
            send(*socket, data, strlen(data), 0);
        data_size = 0;
    }

    fclose(file);
}

// SELECT [column1, column2, ......] FROM [table_name]
void selectColFrom(int *socket, const char *db, const char *tb, const char col[128][64], int col_size) {
    char buffer[256];
    sprintf(buffer, "%s/%s/%s.tb", "databases", db, tb);
    FILE *file = fopen(buffer, "r");
    if (!file)
        return;

    char data[4096], tb_col[64], tb_col_size = 0;
    bool col_reserved[128];

    memset(col_reserved, 0, sizeof(col_reserved));
    memset(data, 0, sizeof(data));

    int col_num = 0;
    char ch;

    // Read header
    while (fscanf(file, "%c", &ch) != EOF) {
        if (ch == ',') {
            tb_col[tb_col_size++] = '\0';
            if (isInCol(tb_col, col, col_size)) {
                col_reserved[col_num] = 1;

                if (col_size == 1) {
                    strcat(data, tb_col);
                    strcat(data, " "); // Tambahkan spasi setelah satu kolom
                } else {
                    strcat(data, tb_col);
                    strcat(data, " ");
                }
            }
            tb_col[0] = '\0';
            tb_col_size = 0;
            col_num++;
        } else if (ch == '\n') {
            strcat(data, "\n");
            send(*socket, data, strlen(data), 0);
            break;
        } else {
            tb_col[tb_col_size++] = ch;
        }
    }

    col_num = 0;
    memset(data, 0, sizeof(data));

    // Read data
    while (fscanf(file, "%c", &ch) != EOF) {
        if (ch == ',') {
            tb_col[tb_col_size++] = '\0';
            if (col_reserved[col_num++]) {
                strcat(data, tb_col);
                strcat(data, " ");
            }
            tb_col[0] = '\0';
            tb_col_size = 0;
        } else if (ch == '\n') {
            col_num = 0;
            strcat(data, "\n");
            send(*socket, data, strlen(data), 0);
            memset(data, 0, sizeof(data));
        } else {
            tb_col[tb_col_size++] = ch;
        }
    }
    strcat(data, "\n");
    send(*socket, data, strlen(data), 0);
    memset(data, 0, sizeof(data));
    fclose(file);
}

// SELECT [column1, column2, ......] FROM [table_name] WHERE [column=value]
void selectColFromWhere(int *socket, const char *db, const char *tb, const char col[128][64], int col_size, const char *where_col, const char *val)
{
    char buff[256];
    sprintf(buff, "%s/%s/%s.tb", "databases", db, tb);
    FILE *file = fopen(buff, "r");
    if (!file)
        return;

    char data[4096], tb_col[64], tb_col_size = 0;
    bool tb_col_reserved[128];

    memset(tb_col_reserved, 0, sizeof(tb_col_reserved));
    memset(data, 0, sizeof(data));

    int tb_col_number = 0;
    char ch, cell_name_i[256];
    int cell_name_size_i = 0, col_number = 1;
    bool col_found = false;

    // Read header
    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[tb_col_size++] = '\0';
            cell_name_i[cell_name_size_i] = '\0';
            cell_name_size_i = 0;

            // Check if condition column is found
            if (!col_found)
            {
                if (strcmp(cell_name_i, where_col) == 0)
                    col_found = true;
                else
                    col_number++;
            }

            if (isInCol(tb_col, col, col_size))
            {
                tb_col_reserved[tb_col_number] = 1;
                if (col_size == 1)
                {
                    strcat(data, tb_col);
                    strcat(data, " ");
                }
                else
                {
                    strcat(data, tb_col);
                    strcat(data, " ");
                }
            }
            tb_col[0] = '\0';
            tb_col_size = 0;
            tb_col_number++;

            memset(cell_name_i, 0, sizeof(cell_name_i));
        }
        else if (ch == '\n')
        {
            strcat(data, "\n");
            send(*socket, data, strlen(data), 0);
            break;
        }
        else
        {
            tb_col[tb_col_size++] = ch;
            cell_name_i[cell_name_size_i++] = ch;
        }
    }

    tb_col_number = 0;
    memset(data, 0, sizeof(data));

    int col_num_i = 1;
    bool isValid = false;

    // Read content
    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[tb_col_size++] = '\0';
            cell_name_i[cell_name_size_i] = '\0';
            cell_name_size_i = 0;

            if (col_num_i == col_number)
            {
                if (strcmp(cell_name_i, val) == 0)
                    isValid = true;
            }

            if (tb_col_reserved[tb_col_number++])
            {
                strcat(data, tb_col);
                strcat(data, " ");
            }

            tb_col[0] = '\0';
            tb_col_size = 0;

            col_num_i++;
            memset(cell_name_i, 0, sizeof(cell_name_i));
        }
        else if (ch == '\n')
        {
            col_num_i = 1;
            tb_col_number = 0;
            strcat(data, "\n");
            if (isValid)
                send(*socket, data, strlen(data), 0);

            isValid = false;
            memset(data, 0, sizeof(data));
        }
        else
        {
            tb_col[tb_col_size++] = ch;
            cell_name_i[cell_name_size_i++] = ch;
        }
    }

    strcat(data, "\n");
    if (isValid)
        send(*socket, data, strlen(data), 0);

    memset(data, 0, sizeof(data));
    fclose(file);
}

// UPDATE [table_name] SET [column = value]
int updateSet(const char *db, const char *tb, char col[64], char val[])
{
    char buffer[256], new_buffer[256];
    sprintf(buffer, "%s/%s/%s.tb", "databases", db, tb);
    sprintf(new_buffer, "%s/%s/%s_updated.tb", "databases", db, tb);

    FILE *file = fopen(buffer, "r");
    if (!file)
        return 0;
    FILE *new_file = fopen(new_buffer, "w");
    if (!new_file)
        return 0;

    char tb_col[64];
    int col_size = 0, col_id = 0, col_num = 0;
    char ch;

    // Read header
    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[col_size++] = '\0';
            if (strcmp(tb_col, col) == 0)
                col_id = col_num;

            fprintf(new_file, "%s,", tb_col);
            tb_col[0] = '\0';
            col_size = 0;
            col_num++;
        }

        else if (ch == '\n')
        {
            fprintf(new_file, "\n");
            break;
        }

        else
            tb_col[col_size++] = ch;
    }
    col_num = 0;

    // Read content
    int affected = 0;
    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[col_size++] = '\0';
            if ((col_num++) == col_id)
            {
                fprintf(new_file, "%s,", val);
                affected++;
            }
            else
            {
                fprintf(new_file, "%s,", tb_col);
            }
            tb_col[0] = '\0';
            col_size = 0;
        }
        else if (ch == '\n')
        {
            col_num = 0;
            fprintf(new_file, "\n");
        }
        else
            tb_col[col_size++] = ch;
    }

    fclose(new_file);
    fclose(file);

    remove(buffer);
    rename(new_buffer, buffer);
    return affected;
}

// UPDATE [table_name] SET [column = value] WHERE [column = value]
int updateSetWhere(const char *db, const char *tb, char col[64], char val[], char *old_val, char *where_col)
{
    char buffer[256], new_buffer[256];
    sprintf(buffer, "%s/%s/%s.tb", "databases", db, tb);
    sprintf(new_buffer, "%s/%s/%s_updated.tb", "databases", db, tb);

    FILE *file = fopen(buffer, "r");
    if (!file)
        return 0;
    FILE *new_file = fopen(new_buffer, "w");
    if (!new_file)
        return 0;

    char tb_col[64], ch;
    int col_size = 0, col_id = 0, col_num = 0;

    // Read header
    char col_i[256];
    int col_size_i = 0, col_number = 1;
    bool col_found = false;
    memset(col_i, 0, sizeof(col_i));

    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[col_size++] = '\0';
            col_i[col_size_i] = '\0';
            if ((strcmp(col_i, where_col) == 0) && !col_found)
                col_found = true;

            if (strcmp(tb_col, col) == 0)
                col_id = col_num;
            fprintf(file, "%s,", tb_col);
            tb_col[0] = '\0';
            col_size = 0;
            col_num++;

            if (!col_found)
                col_number++;
            col_size_i = 0;
            memset(col_i, 0, sizeof(col_i));
        }
        else if (ch == '\n')
        {
            fprintf(new_file, "\n");
            break;
        }
        else
        {
            tb_col[col_size++] = ch;
            col_i[col_size_i++] = ch;
        }
    }
    col_num = 0;

    // Read content
    col_size_i = 0;
    int col_num_i = 1;
    memset(col_i, 0, sizeof(col_i));
    char temp_cell[256];
    memset(temp_cell, 0, sizeof(temp_cell));
    int affected = 0;
    bool isValid = false;
    char data[256];
    char invalid_data[256];

    memset(data, 0, sizeof(data));
    memset(invalid_data, 0, sizeof(invalid_data));

    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[col_size++] = '\0';
            col_i[col_size_i] = '\0';

            if ((strcmp(old_val, col_i) == 0) && (col_number == col_num_i))

                isValid = true;

            if (((col_num++) == col_id))
            {
                sprintf(temp_cell, "%s,", val);
                strcat(data, temp_cell);
            }
            else
            {
                sprintf(temp_cell, "%s,", tb_col);
                strcat(data, temp_cell);
            }

            sprintf(temp_cell, "%s,", tb_col);
            strcat(invalid_data, temp_cell);

            tb_col[0] = '\0';
            col_size = 0;

            col_size_i = 0;
            col_num_i++;
            memset(col_i, 0, sizeof(col_i));
        }
        else if (ch == '\n')
        {
            // New line
            if (isValid)
            {
                fprintf(new_file, "%s", data);
                fprintf(new_file, "\n");
                affected++;
            }
            else
            {
                fprintf(new_file, "%s", invalid_data);
                fprintf(new_file, "\n");
            }

            memset(data, 0, sizeof(data));
            memset(invalid_data, 0, sizeof(invalid_data));
            col_num_i = 1;
            col_num = 0;
            isValid = false;
        }
        else
        {
            tb_col[col_size++] = ch;
            col_i[col_size_i++] = ch;
        }
    }

    if (isValid)
    {
        fprintf(new_file, "%s", data);
        affected++;
    }
    else
        fprintf(new_file, "%s", invalid_data);
    fclose(new_file);
    fclose(file);
    remove(buffer);
    rename(new_buffer, buffer);
    return affected;
}

// DELETE FROM
int deleteFrom(char *db, char *tb, char col[], char val[])
{
    char buffer[256], new_buffer[256];
    sprintf(buffer, "%s/%s/%s.tb", "databases", db, tb);
    sprintf(new_buffer, "%s/%s/%s_updated.tb", "databases", db, tb);

    FILE *file = fopen(buffer, "r");
    if (!file)
        return 0;
    FILE *new_file = fopen(new_buffer, "w");
    if (!new_file)
        return 0;

    char tb_col[64], ch;
    int col_size = 0, col_id = 0, col_num = 0;
    bool where = true;

    // '#' means query is not using "WHERE"
    if (strcmp(col, "#") == 0)
        where = false;

    // Read header
    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[col_size++] = '\0';
            if (where && strcmp(tb_col, col) == 0)
                col_id = col_num;

            fprintf(new_file, "%s,", tb_col);
            tb_col[0] = '\0';
            col_size = 0;
            col_num++;
        }
        else if (ch == '\n')
        {
            // New line is detected
            fprintf(new_file, "\n");
            break;
        }
        else
            tb_col[col_size++] = ch;
    }
    col_num = 0;

    // Read content
    int affected = 0;
    char data[4096];
    memset(data, 0, sizeof(data));
    bool deleted = false;
    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[col_size++] = '\0';
            if (!where)
            {
                deleted = true;
                if ((col_num++) == col_id)
                    affected++;
            }
            else
            {
                if ((col_num++) == col_id && strcmp(tb_col, val) == 0)
                {
                    deleted = true;
                    affected++;
                }
            }
            strcat(data, tb_col);
            strcat(data, ",");
            tb_col[0] = '\0';
            col_size = 0;
        }
        else if (ch == '\n')
        {
            // New line
            col_num = 0;
            strcat(data, "\n");
            if (!deleted)
            {
                fprintf(new_file, "%s", data);
            }
            memset(data, 0, sizeof(data));
            deleted = false;
        }
        else
            tb_col[col_size++] = ch;
    }

    strcat(data, "\n");
    if (!deleted)
        fprintf(new_file, "%s", data);

    memset(data, 0, sizeof(data));
    deleted = false;
    fclose(new_file);
    fclose(file);
    remove(buffer);
    rename(new_buffer, buffer);
    return affected;
}

// Directory traversal to find correct database
int findDB(const char *path)
{
    DIR *dir = opendir(path);
    size_t path_len = strlen(path);
    int r = -1;

    if (dir)
    {
        struct dirent *dp;

        r = 0;
        while (!r && (dp = readdir(dir)))
        {
            int r2 = -1;
            char *buffer;
            size_t length;

            if (!strcmp(dp->d_name, ".") || !strcmp(dp->d_name, ".."))
                continue;

            length = path_len + strlen(dp->d_name) + 2;
            buffer = malloc(length);

            if (buffer)
            {
                struct stat statbuf;

                snprintf(buffer, length, "%s/%s", path, dp->d_name);
                if (!stat(buffer, &statbuf))
                {
                    if (S_ISDIR(statbuf.st_mode))
                        r2 = findDB(buffer);
                    else
                        r2 = unlink(buffer);
                }
                free(buffer);
            }
            r = r2;
        }
        closedir(dir);
    }

    if (!r)
        r = rmdir(path);

    return r;
}

void dropDB(char *db)
{
    char path[50];
    sprintf(path, "%s/%s", "databases", db);
    findDB(path);
}

// DROP TABLE [table_name]
void dropTB(int *socket, char *db, char *tb)
{
    char path[50];
    sprintf(path, "%s/%s/%s.tb", "databases", db, tb);
    char message[1024];

    if (remove(path) == 0)
    {
        sprintf(message, "Table %s.%s has been dropped.\n", db, tb);
        send(*socket, message, strlen(message), 0);
    }
    else
    {
        strcpy(message, "Error! Table failed to drop\n");
        send(*socket, message, strlen(message), 0);
    }
}

// DROP COLUMN
int dropColumn(const char *db, const char *tb, char col[64], char val[])
{
    char buffer[256], new_buffer[256];
    sprintf(buffer, "%s/%s/%s.tb", "databases", db, tb);
    sprintf(new_buffer, "%s/%s/%s_updated.tb", "databases", db, tb);
    FILE *file = fopen(buffer, "r");
    if (!file)
        return 0;
    FILE *new_file = fopen(new_buffer, "w");
    if (!new_file)
        return 0;

    char tb_col[64], ch;
    int col_size = 0, col_id = 0, col_num = 0;

    // Read header
    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[col_size++] = '\0';

            if (strcmp(tb_col, col) == 0)
            {
                col_id = col_num;
                fprintf(new_file, "%s", val);
            }
            else
                fprintf(new_file, "%s,", tb_col);

            tb_col[0] = '\0';
            col_size = 0;
            col_num++;
        }
        else if (ch == '\n')
        {
            fprintf(new_file, "\n");
            break;
        }
        else
            tb_col[col_size++] = ch;
    }
    col_num = 0;

    // Read content
    int affected = 0;
    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == ',')
        {
            tb_col[col_size++] = '\0';
            if ((col_num++) == col_id)
            {
                fprintf(new_file, "%s", val);
                affected++;
            }
            else
            {
                fprintf(new_file, "%s,", tb_col);
            }
            tb_col[0] = '\0';
            col_size = 0;
        }
        else if (ch == '\n')
        {
            col_num = 0;
            fprintf(new_file, "\n");
        }
        else
            tb_col[col_size++] = ch;
    }
    fclose(new_file);
    fclose(file);
    remove(buffer);
    rename(new_buffer, buffer);
    return affected;
}

//-------Authentication-------//

// Set current user
void setUser(User *user, char *username, char *password)
{
    strcpy(user->username, username);
    strcpy(user->password, password);
    return;
}

void readUserTB(User *user, char *line)
{
    char username[50];
    char password[50];

    int i = 0, j = 0;

    while (line[i] != ',')
        username[j++] = line[i++];

    username[j++] = '\0';
    i++;
    strcpy(user->username, username);

    j = 0;
    while (line[i] != ',')
    {
        password[j++] = line[i++];
    }
    password[j++] = '\0';
    strcpy(user->password, password);
}

// Check if user is equal
bool isEqual(User *a, User *b)
{
    return (strcmp(a->username, b->username) == 0 && strcmp(a->password, b->password) == 0);
}

// Check if user and password match
bool scanUser(User *user)
{
    char path[256];
    sprintf(path, "%s/%s/%s.tb", "databases", "userDB", "users");
    FILE *file = fopen(path, "r");

    if (!file)
        return false;

    User check;
    char temp[256];
    int t_size = 0;
    char ch;
    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == '\n')
        {
            if (t_size)
            {
                temp[t_size++] = '\n';
                temp[t_size++] = '\0';
                readUserTB(&check, temp);
                if (isEqual(user, &check))
                    return true;

                t_size = 0;
            }
        }
        else
            temp[t_size++] = ch;
    }

    if (t_size)
    {
        temp[t_size++] = '\n';
        temp[t_size++] = '\0';
        readUserTB(&check, temp);
        if (isEqual(user, &check))
            return true;

        t_size = 0;
    }
    fclose(file);

    return false;
}

// Check current user
bool checkUser(User *user)
{
    if (strcmp(user->username, "root") == 0)
    {
        user->isRoot = true;
        return true;
    }
    else if (scanUser(user))
    {
        user->isRoot = false;
        return true;
    }

    return false;
}

// Grant access to database
void grantPermission(char *db, char *username)
{
    char *attribute[64];
    attribute[0] = db;
    attribute[1] = username;
    insertInto("userDB", "permission", attribute, 2);
}

void __hasPermissionToDBHelper(char *line, char *db_r, char *us_r)
{
    char db[50], user[50];
    int i = 0, j = 0;

    while (line[i] != ',')
        db[j++] = line[i++];
    db[j++] = '\0';

    i++;
    strcpy(db_r, db);
    j = 0;

    while (line[i] != ',')
        user[j++] = line[i++];
    user[j++] = '\0';

    strcpy(us_r, user);
}

// Check if user has permission to database
bool isAllowedToDB(char *username, char *db)
{
    // Root has permission to all databases (except userDB)
    if (strcmp(username, "root") == 0)
        return true;

    char path[256];
    sprintf(path, "%s/%s/%s.tb", "databases", "userDB", "permission");
    FILE *file = fopen(path, "r");

    // File not exist
    if (!file)
        return false;

    int temp_size = 0;
    char temp[256], ch, db_read[50], us_read[50];

    while (fscanf(file, "%c", &ch) != EOF)
    {
        if (ch == '\n')
        {
            if (temp_size)
            {
                temp[temp_size++] = '\n';
                temp[temp_size++] = '\0';
                __hasPermissionToDBHelper(temp, db_read, us_read);

                // Database and username match
                if (strcmp(db_read, db) == 0 && strcmp(us_read, username) == 0)
                    return true;

                temp_size = 0;
            }
        }
        else
            temp[temp_size++] = ch;
    }

    if (temp_size)
    {
        temp[temp_size++] = '\n';
        temp[temp_size++] = '\0';
        __hasPermissionToDBHelper(temp, db_read, us_read);

        //
        if (strcmp(db_read, db) == 0 && strcmp(us_read, username) == 0)
            return true;

        temp_size = 0;
    }
    fclose(file);

    return false;
}

// Create new user
void createUser(char *username, char *password)
{
    char *attribute[64];
    attribute[0] = username;
    attribute[1] = password;
    insertInto("userDB", "users", attribute, 2);
}

//-------Log-------//

// Get Current Time
char *getCurTime()
{
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char *time;
    time = (char *)malloc(100);
    sprintf(time, "%d-%02d-%02d %02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    return time;
}

// Write log
void writeLog(User *user, char *command)
{
    char currTime[30];
    strcpy(currTime, getCurTime());

    char log[1000];
    sprintf(log, "%s:%s:%s", currTime, user->username, command);

    FILE *file = fopen("database.log", "a");
    fprintf(file, "%s\n", log);
    fclose(file);
}

//-------Dump-------//

// Scan table
void scanTB(int *socket, char db[], char tb[])
{
    char path[256];
    sprintf(path, "%s/%s/%s.tb", "databases", db, tb);
    FILE *file = fopen(path, "r");
    if (file)
    {
        char buffer[2048], data[1024], temp[64], ch;
        int t_size = 0;
        int new_socket = *(int *)socket;

        memset(data, 0, sizeof(data));

        // Read table information file
        char createPath[256];
        sprintf(createPath, "%s/%s/.%s_tb", "databases", db, tb);

        FILE *createFile = fopen(createPath, "r");
        if (createFile)
        {
            memset(data, 0, sizeof(data));
            int i = 0;
            while (fscanf(createFile, "%c", &ch) != EOF)
                data[i++] = ch;

            data[i++] = '\0';
            sprintf(buffer, "%s\n", data);
            memset(data, 0, sizeof(data));

            send(new_socket, buffer, strlen(buffer), 0);
            fclose(createFile);
        }

        // Read header
        while (fscanf(file, "%c", &ch) != EOF)
        {
            if (ch == '\n')
                break;
        }

        memset(data, 0, sizeof(data));

        strcpy(data, "INSERT INTO ");
        strcat(data, tb);
        strcat(data, " (");

        // Read content
        while (fscanf(file, "%c", &ch) != EOF)
        {
            if (ch == ',')
            {
                temp[t_size++] = '\0';
                strcat(data, temp);
                strcat(data, ", ");

                temp[0] = '\0';
                t_size = 0;
            }

            else if (ch == '\n')
            {
                // Replace ", " with ");"
                data[strlen(data) - 1] = ';';
                data[strlen(data) - 2] = ')';

                sprintf(buffer, "%s\n", data);
                send(new_socket, buffer, strlen(buffer), 0);

                memset(data, 0, sizeof(data));

                strcpy(data, "INSERT INTO ");
                strcat(data, tb);
                strcat(data, " (");
            }
            else
                temp[t_size++] = ch;
        }

        // Replace ", " with ");"
        data[strlen(data) - 1] = ';';
        data[strlen(data) - 2] = ')';

        sprintf(buffer, "%s\n", data);
        send(new_socket, buffer, strlen(buffer), 0);

        memset(data, 0, sizeof(data));

        fclose(file);
    }
}

// Scan database
void scanDB(int *socket, char db[])
{
    DIR *dir;
    struct dirent *dp;

    char path[256];
    sprintf(path, "%s/%s", "databases", db);

    dir = opendir(path);

    if (dir)
    {
        int new_socket = *(int *)socket;

        char buffer[1024];
        memset(buffer, 0, sizeof(buffer));
        sprintf(buffer, "CREATE DATABASE %s;\n", db);

        send(new_socket, buffer, strlen(buffer), 0);

        // Directory traversal
        while ((dp = readdir(dir)))
        {
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
            {
                char tb[64];
                strcpy(tb, dp->d_name);

                // Remove .tb extension from string table_name
                tb[strlen(tb) - 3] = '\0';
                scanTB(socket, db, tb);
            }
        }

        close(new_socket);
        closedir(dir);
    }
}

//-------Extras-------//

// Directory traversal
void listFilesRecursively(char *basePath, char *data)
{
    char path[1024];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            char ch, tb[1024];
            int i = 0;

            strcat(data, dp->d_name);
            strcat(data, "#");

            FILE *file = fopen(dp->d_name, "r");
            while ((ch = fgetc(file)) != EOF)
            {
                tb[i] = ch;
                i++;
            }
            fclose(file);
            tb[i] = '#';
            strcat(data, tb);

            // Construct new path from our base path
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);

            listFilesRecursively(path, data);
        }
    }

    closedir(dir);
}

// DOWNLOAD DATABASE [database_name]
void downloadDB(int *socket, char db[])
{
    char path[1024], data[2048] = "";
    strcat(data, db);
    strcat(data, "#");

    struct dirent *dp;
    sprintf(path, "%s/%s", "databases", db);
    DIR *dir = opendir(db);

    if (!dir)
    {
        char message[1024] = "Failed to download databases!\n";
        send(*socket, message, strlen(message), 0);
        return;
    }
    listFilesRecursively(path, data);
    closedir(dir);
    send(*socket, data, strlen(data), 0);
}

int main(int argc, char const *argv[])
{
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    // Users database: Users table and Permission Table
    if (access("databases/userDB/users.tb", F_OK) != 0)
    {
        createDB("userDB");
    }
    char *attribute[64];
    attribute[0] = "username";
    attribute[1] = "password";
    if (!tbExist("userDB", "users"))
    {
        // Data type: (string, string)
        char dt[2][32] = {"string", "string"};
        createTB("userDB", "users", attribute, 2, dt, 2);
    }
    attribute[0] = "database";
    attribute[1] = "user";
    if (!tbExist("userDB", "permission"))
    {
        // Data type: (string, string)
        char dt[2][32] = {"string", "string"};
        createTB("userDB", "permission", attribute, 2, dt, 2);
    }

    // Loop command
    while (1)
    {
        // read variable
        char buffer[1024] = {0};
        char username[100], password[100];
        User current;

        // check user -> authentication
        valread = read(new_socket, buffer, 1024);
        if (strcmp(buffer, "root") == 0)
        {
            setUser(&current, buffer, buffer);
            char message[1024] = "Logged in as root!\n";
            send(new_socket, message, strlen(message), 0);
        }
        else if (strcmp(buffer, "error") == 0)
        {
            close(new_socket);
            return 0;
        }

        else
        {
            char username[30];
            char password[30];

            strcpy(username, buffer);
            memset(buffer, 0, sizeof(buffer));

            char message[1024] = "username received\n";
            send(new_socket, message, strlen(message), 0);

            valread = read(new_socket, buffer, 1024);
            strcpy(password, buffer);

            setUser(&current, username, password);
        }
        memset(buffer, 0, sizeof(buffer));

        if (!checkUser(&current))
        {
            char message[1024] = "authentication error";
            send(new_socket, message, strlen(message), 0);
            close(new_socket);
            return 0;
        }

        // Current Database
        char currentDB[128];
        currentDB[0] = '\0';

        // All available operations from client
        while (1)
        {
            valread = read(new_socket, buffer, 1024);

            // exit program
            if (strcmp(buffer, "exit") == 0)
            {
                printf("Closing client\n");
                char message[1024] = "Program closed...\n";
                send(new_socket, message, strlen(message), 0);
                close(new_socket);
                return 0;
            }

            // Commands received from clients
            char com[128][64];
            int command_size = 0;
            splitCommands(buffer, com, &command_size);

            // 1st argument: CREATE
            if (strcmp(com[0], "CREATE") == 0)
            {
                // 2nd argument: DATABASE -> CREATE DATABASE
                if (strcmp(com[1], "DATABASE") == 0)
                {
                    // 3rd argument: [database_name] -> CREATE DATABASE [database_name]
                    if (strlen(com[2]) > 0)
                    {
                        // Database exist
                        if (dbExist(com[2]))
                        {
                            char message[1024] = "Error: Database already exists.\n";
                            send(new_socket, message, strlen(message), 0);
                        }
                        // Create database
                        else
                        {
                            createDB(com[2]);
                            grantPermission(com[2], current.username);
                            char message[1024] = "Database successfully created!\n";
                            send(new_socket, message, strlen(message), 0);
                            writeLog(&current, buffer);
                        }
                    }
                    else
                    {
                        char message[1024] = "Syntax Error: CREATE DATABASE [database_name]\n";
                        send(new_socket, message, strlen(message), 0);
                    }
                }

                // 2nd argument: TABLE -> CREATE TABLE
                else if (strcmp(com[1], "TABLE") == 0)
                {
                    // No database is selected yet
                    if (currentDB[0] == '\0')
                    {
                        char message[1024] = "No database selected yet, select one database: USE [database_name].\n";
                        send(new_socket, message, strlen(message), 0);
                    }
                    else
                    {
                        // 3rd to @command_size argument: [table_name] (attribute1 data_type, attribute2 data_type)
                        // -> CREATE TABLE [table_name] (attribute1 data_type, attribute2 data_type)
                        char *attribute[64], dt[128][32];
                        int i = 0, dt_size = 0, j = 0;
                        for (int j = 3; j < command_size; j += 2)
                        {
                            attribute[i++] = com[j];
                            strcpy(dt[dt_size++], com[j + 1]);
                        }

                        if (!tbExist(currentDB, com[2]))
                        {
                            createTB(currentDB, com[2], attribute, i, dt, dt_size);
                            writeLog(&current, buffer);
                            char message[1024] = "Table created\n";
                            send(new_socket, message, strlen(message), 0);
                        }
                        else
                        {
                            char message[1024] = "Error: Table already exist!\n";
                            send(new_socket, message, strlen(message), 0);
                        }
                    }
                }

                // 2nd argument: USER -> CREATE USER
                else if (strcmp(com[1], "USER") == 0)
                {
                    // Regular user is not allowed to create new user
                    if (!current.isRoot)
                    {
                        char message[1024] = "Permission denied!\n";
                        send(new_socket, message, strlen(message), 0);
                    }

                    // Current user is root
                    else if (current.isRoot)
                    {
                        // Syntax error
                        if (command_size != 6 || strcmp(com[3], "IDENTIFIED") != 0 || strcmp(com[4], "BY") != 0)
                        {
                            char message[1024] = "Syntax error: CREATE USER [username] IDENTIFIED BY [password]\n";
                            send(new_socket, message, strlen(message), 0);
                        }

                        // Create new user -> CREATE USER [username] IDENTIFIED BY [password]
                        else
                        {
                            createUser(com[2], com[5]);
                            char message[1024];
                            sprintf(message, "User %s has been created!\n", com[2]);
                            writeLog(&current, buffer);
                            send(new_socket, message, strlen(message), 0);
                        }
                    }
                }

                // 2nd argument: ERROR
                else
                {
                    char message[1024] = "Syntax Error: CREATE [DATABASE | TABLE | USER]\n";
                    send(new_socket, message, strlen(message), 0);
                }
            }

            // 1st argument: USE
            else if (strcmp(com[0], "USE") == 0)
            {
                // 2nd argument: [database_name] -> USE [database_name]
                if (command_size != 2)
                {
                    char message[1024] = "Syntax error! Database name must be only 1 word: USE [database_name]\n";
                    send(new_socket, message, strlen(message), 0);
                }
                else if (dbExist(com[1]))
                {
                    // User database is not allowed to be change, even by root
                    if (strcmp(com[1], "userDB") == 0)
                    {
                        char message[1024] = "Permission denied!\n";
                        send(new_socket, message, strlen(message), 0);
                    }

                    else if (isAllowedToDB(current.username, com[1]))
                    {
                        strcpy(currentDB, com[1]);
                        writeLog(&current, buffer);
                        char message[1024];
                        sprintf(message, "%s database selected!\n", com[1]);
                        send(new_socket, message, strlen(message), 0);
                    }
                    else
                    {
                        char message[1024] = "Permission denied! You dont have access to that database!\n";
                        send(new_socket, message, strlen(message), 0);
                    }
                }
                else
                {
                    char message[1024] = "Error, database not found!\n";
                    send(new_socket, message, strlen(message), 0);
                }
            }

            // 1st argument: INSERT
            else if (strcmp(com[0], "INSERT") == 0)
            {
                // 2nd argument: INTO -> INSERT INTO
                if (strcmp(com[1], "INTO") == 0)
                {
                    // No database is selected yet
                    if (currentDB[0] == '\0')
                    {
                        char message[1024] = "No database selected yet, select one database: USE [database_name].\n";
                        send(new_socket, message, strlen(message), 0);
                    }

                    // 3rd to @command_size argument: [table_name] (attribute1, attribute2, .......)
                    // -> INSERT INTO [table_name] (attribute1, attribute2, .......)
                    else if (command_size <= 3)
                    {
                        char message[1024] = "Syntax error: INSERT INTO [database_name] (attribute1, attribute2, .......)\n";
                        send(new_socket, message, strlen(message), 0);
                    }
                    else
                    {
                        char buffer[1024], data[1024], content[128][64], ch;
                        int i = 0, content_size = 0;
                        sprintf(buffer, "%s/%s/.%s_tb", "databases", currentDB, com[2]);
                        FILE *file = fopen(buffer, "r");

                        if (file)
                        {
                            while (fscanf(file, "%c", &ch) != EOF)
                            {
                                data[i] = ch;
                                i++;
                            }
                            data[i] = '\0';
                        }
                        splitCommands(data, content, &content_size);

                        // Remove 1-3rd argument: CREATE TABLE [table_name]
                        content_size -= 3;

                        // Count number of parameter
                        content_size /= 2;

                        // Deny command if number of parameter mismatch
                        if ((command_size - 3) < content_size)
                        {
                            char message[1024];
                            sprintf(message, "Too many arguments for table %s parameter!\n", com[2]);
                            send(new_socket, message, strlen(message), 0);
                        }
                        else if ((command_size - 3) > content_size)
                        {
                            char message[1024];
                            sprintf(message, "Too few arguments for table %s parameter!\n", com[2]);
                            send(new_socket, message, strlen(message), 0);
                        }

                        char *attribute[64];
                        i = 0;
                        for (int j = 3; j < command_size; j++)
                            attribute[i++] = com[j];

                        insertInto(currentDB, com[2], attribute, i);
                        writeLog(&current, buffer);

                        char message[1024] = "Data inserted, 1 row(s) affected\n";
                        send(new_socket, message, strlen(message), 0);
                    }
                }
                else
                {
                    char message[1024] = "Usage: INSERT INTO [database_name] (attribute1, attribute2, .......)\n";
                    send(new_socket, message, strlen(message), 0);
                }
            }

            // 1st argument: SELECT
            else if (strcmp(com[0], "SELECT") == 0)
            {
                // No database is selected yet
                if (currentDB[0] == '\0')
                {
                    char message[1024] = "No database selected yet, select one database: USE [database_name].\n";
                    send(new_socket, message, strlen(message), 0);
                }
                else
                {
                    // 2nd argument: * -> SELECT *
                    if (strcmp(com[1], "*") == 0)
                    {
                        // 3rd & 4th argument: FROM [table_name] -> SELECT * FROM [table_name]
                        if (command_size >= 4)
                        {
                            if (command_size == 4)
                            {
                                selectAll(&new_socket, currentDB, com[3]);
                                writeLog(&current, buffer);
                            }

                            // 5th argument: WHERE -> SELECT * FROM [table_name] WHERE
                            else if (strcmp(com[4], "WHERE") == 0)
                            {
                                char col_name[64], value[64];

                                // 6th argument: [condition] -> SELECT * FROM [table_name] WHERE [column_name=value]
                                if (command_size == 6)
                                {
                                    int i = 0;
                                    // Get column name
                                    while (com[5][i] != '=')
                                    {
                                        col_name[i] = com[5][i];
                                        i++;
                                    }
                                    col_name[i] = '\0';

                                    i++;

                                    // Get value
                                    int j = 0;
                                    while (com[5][i] != '\0')
                                    {
                                        value[j] = com[5][i];
                                        i++;
                                        j++;
                                    }
                                    value[j] = '\0';
                                }

                                // 6th, 7th, and 8th argument: [condition] -> SELECT * FROM [table_name] WHERE [column_name = value]
                                else if (command_size == 8)
                                {
                                    strcpy(col_name, com[5]);
                                    strcpy(value, com[7]);
                                }

                                //
                                selectAllWhere(&new_socket, currentDB, com[3], col_name, value);
                                writeLog(&current, buffer);
                            }
                        }
                        // Syntax error
                        else
                        {
                            char message[1024] = "Syntax error: SELECT [column1, column2 | * ] FROM [table_name] (WHERE [column=value])\n";
                            send(new_socket, message, strlen(message), 0);
                        }
                    }

                    // 2nd argument: column 1, column2, ..... -> SELECT [column1, column2, .....]
                    else
                    {
                        // Selected columns
                        char cols[128][64];
                        int col_size = 0;

                        // Condition
                        char tb[64], value[64];

                        // Incremental value
                        int i = 1;
                        bool isValid = false, where = false;
                        while (i < command_size)
                        {
                            // All columns already selected, next argument: FROM -> SELECT [column1, column2, ......] FROM
                            if (strcmp(com[i], "FROM") == 0)
                            {
                                // Next argument: [table_name] -> SELECT [column1, column2, ......] FROM [table_name]
                                if (i + 1 < command_size)
                                {
                                    strcpy(tb, com[i + 1]);
                                    isValid = true;
                                }
                                break;
                            }

                            // Store all selected columns
                            else
                            {
                                strcpy(cols[col_size++], com[i]);
                                i++;
                            }
                        }

                        // Using "WHERE" argument -> SELECT [column1, column2, ......] FROM [table_name] WHERE
                        char condition_col[64];
                        if (strcmp(com[i + 2], "WHERE") == 0)
                        {
                            where = true;
                            int k = 0;

                            // SELECT [column1, column2, ......] FROM [table_name] WHERE [column_name=value]
                            if (command_size == i + 4)
                            {
                                // Get column name
                                while (com[i + 3][k] != '=')
                                {
                                    condition_col[k] = com[i + 3][k];
                                    k++;
                                }
                                condition_col[k] = '\0';

                                k++;

                                // Get value
                                int j = 0;

                                while (com[i + 3][k] != '\0')
                                {
                                    value[j] = com[i + 3][k];
                                    k++;
                                    j++;
                                }
                                value[j] = '\0';
                            }

                            // SELECT [column1, column2, ......] FROM [table_name] WHERE [column_name = value]
                            else if (command_size == i + 6)
                            {
                                strcpy(condition_col, com[i + 3]);
                                strcpy(value, com[i + 5]);
                            }
                        }

                        // Syntax error
                        if (!isValid)
                        {
                            char message[1024] = "Syntax error: SELECT [col1, col2, ... | * ] FROM [table_name] (WHERE [column=value])\n";
                            send(new_socket, message, strlen(message), 0);
                        }

                        // SELECT [column1, column2, ......] FROM [table_name]
                        else if (!where)
                        {
                            selectColFrom(&new_socket, currentDB, tb, cols, col_size);
                            writeLog(&current, buffer);
                        }

                        // SELECT [column1, column2, ......] FROM [table_name] WHERE [column=value]
                        if (where && isValid)
                        {
                            selectColFromWhere(&new_socket, currentDB, tb, cols, col_size, condition_col, value);
                            writeLog(&current, buffer);
                        }
                    }
                }
            }

            // 1st argument: UPDATE
            else if (strcmp(com[0], "UPDATE") == 0)
            {
                // 2nd argument: [database_name] -> UPDATE [database_name]
                if (tbExist(currentDB, com[1]))
                {
                    // 3rd argument: SET -> UPDATE [database_name] SET
                    if (strcmp(com[2], "SET") == 0)
                    {
                        // UPDATE [database_name] SET [column = value]
                        if (command_size == 6 && strcmp(com[4], "=") == 0)
                        {
                            int affected = updateSet(currentDB, com[1], com[3], com[5]);
                            char buffer[128];
                            sprintf(buffer, "Database updated, %d row(s) affected.\n", affected);
                            send(new_socket, buffer, strlen(buffer), 0);
                            writeLog(&current, buffer);
                        }

                        // UPDATE [database_name] SET [column = value] WHERE [column = value]
                        else if (command_size == 10 && strcmp(com[8], "=") == 0)
                        {
                            int affected = updateSetWhere(currentDB, com[1], com[3], com[5], com[9], com[7]);
                            char buffer[128];
                            sprintf(buffer, "Database updated, %d row(s) affected.\n", affected);
                            send(new_socket, buffer, strlen(buffer), 0);
                            writeLog(&current, buffer);
                        }
                        else
                        {
                            char message[1024] = "Syntax error: UPDATE [table_name] SET [column = value]\n";
                            send(new_socket, message, strlen(message), 0);
                        }
                    }
                    else
                        send(new_socket, "Syntax error: UPDATE [table_name] SET [column = value]\n",
                             strlen("Syntax error: UPDATE [table_name] SET [column = value]\n"), 0);
                }
                else
                    send(new_socket, "Table not found!\n", strlen("Table not found!\n"), 0);
            }

            // 1st argument: DELETE
            else if (strcmp(com[0], "DELETE") == 0)
            {
                // 2nd argument: FROM -> DELETE FROM
                if (strcmp(com[1], "FROM") == 0)
                {
                    // No database is selected yet
                    if (currentDB[0] == '\0')
                        send(new_socket, "No database selected yet, select one database: USE [database_name].\n",
                             strlen("No database selected yet, select one database: USE [database_name].\n"), 0);

                    // 3rd argument: [table_name] -> DELETE FROM [table_name]
                    else
                    {
                        if (tbExist(currentDB, com[2]))
                        {
                            if (command_size == 7 && strcmp(com[3], "WHERE") == 0 && strcmp(com[5], "=") == 0)
                            {
                                // Delete with WHERE
                                int affected = deleteFrom(currentDB, com[2], com[4], com[6]);
                                char buffer[128];
                                sprintf(buffer, "%d row(s) affected.\n", affected);
                                send(new_socket, buffer, strlen(buffer), 0);
                            }
                            else
                            {
                                // Delete without where
                                int affected = deleteFrom(currentDB, com[2], "#", "#");
                                char buffer[128];
                                sprintf(buffer, "%d row(s) affected.\n", affected);
                                send(new_socket, buffer, strlen(buffer), 0);
                            }
                            writeLog(&current, buffer);
                        }
                        else
                            send(new_socket, "Table not found!\n", strlen("Table not found!\n"), 0);
                    }
                }
                // Syntax error
                else
                    send(new_socket, "Syntax error: DELETE FROM [table_name]\n", strlen("Syntax error: DELETE FROM [table name]\n"), 0);
            }

            // 1st argument: GRANT
            else if (strcmp(com[0], "GRANT") == 0)
            {
                // Syntax error
                if (command_size != 5 || strcmp(com[1], "PERMISSION") != 0 || strcmp(com[3], "INTO") != 0)
                    send(new_socket, "Syntax Error: GRANT PERMISSION [database_name] INTO [username]\n",
                         strlen("Syntax Error: GRANT PERMISSION [database_name] INTO [username]\n"), 0);

                // 2nd, 3rd, 4th, 5th argument: PERMISSION [database_name] INTO [username]
                // -> GRANT PERMISSION [database_name] INTO [username]
                else
                {
                    // Regular user don't have permission to grant access for another users to databases
                    if (!current.isRoot)
                        send(new_socket, "Permission denied!\n", strlen("Permission denied!\n"), 0);

                    else if (current.isRoot)
                    {
                        grantPermission(com[2], com[4]);
                        char message[1024];
                        sprintf(message, "%s now has permission to database %s\n", com[4], com[2]);
                        send(new_socket, message, strlen(message), 0);
                        writeLog(&current, buffer);
                    }
                }
            }

            // 1st argument: DROP
            else if (strcmp(com[0], "DROP") == 0)
            {
                if (command_size > 2)
                {
                    // 2nd argument: DATABASE -> DROP DATABASE
                    if (strcmp(com[1], "DATABASE") == 0)
                    {
                        // 3rd argument: [database_name] -> DROP DATABASE [database_name]
                        if (dbExist(com[2]))
                        {
                            if (isAllowedToDB(current.username, com[2]))
                            {
                                dropDB(com[2]);
                                writeLog(&current, buffer);
                                char message[1024];
                                sprintf(message, "%s database dropped.\n", com[2]);
                                send(new_socket, message, strlen(message), 0);
                            }
                            else
                                send(new_socket, "Permission denied!\n", strlen("Permission denied!\n"), 0);
                        }
                        else
                            send(new_socket, "Database not found!\n", strlen("Database not found!\n"), 0);
                    }

                    // 2nd argument: TABLE -> DROP TABLE
                    else if (strcmp(com[1], "TABLE") == 0)
                    {
                        // 3rd argument: [table_name] -> DROP TABLE [table_name]
                        if (tbExist(currentDB, com[2]))
                        {
                            dropTB(&new_socket, currentDB, com[2]);
                            writeLog(&current, buffer);
                        }
                        else
                            send(new_socket, "Table not found!\n", strlen("Table not found!\n"), 0);
                    }

                    // 2nd argument: COLUMN -> DROP COLUMN
                    else if (strcmp(com[1], "COLUMN") == 0)
                    {
                        // Syntax error
                        if (command_size != 5)
                            send(new_socket, "Syntax Error: DROP COLUMN [column_name] FROM [table_name]\n",
                                 strlen("Syntax Error: DROP COLUMN [column_name] FROM [table_name]\n"), 0);

                        // 3rd, 4th, and 5th argument: [column] FROM [table_name]
                        // -> DROP COLUMN [column] FROM [table_name]
                        else
                        {
                            dropColumn(currentDB, com[4], com[2], "");
                            writeLog(&current, buffer);
                        }
                    }
                }
                // Syntax error
                else
                {
                    char message[1024] = "Syntax Error: DROP [DATABASE | TABLE | COLUMN]\n";
                    send(new_socket, message, strlen(message), 0);
                }
            }

            // 1st and 2nd argument: BACKUP DATABASE
            else if (command_size == 3 && strcmp(com[0], "BACKUP") == 0 && strcmp(com[1], "DATABASE") == 0)
            {
                // 3rd argument: [database_name] -> BACKUP DATABASE [database_name]
                if (dbExist(com[2]))
                {
                    scanDB(&new_socket, com[2]);
                    writeLog(&current, buffer);
                }

                else
                {
                    char message[1024] = "Database not found!\n";
                    send(new_socket, message, strlen(message), 0);
                }
            }
            // 1st and 2nd argument: DOWNLOAD DATABASE
            else if (command_size == 3 && strcmp(com[0], "DOWNLOAD") == 0 && strcmp(com[1], "DATABASE") == 0)
            {
                // 3rd argument: [database_name] -> DOWNLOAD DATABASE [database_name]
                if (dbExist(com[2]))
                {
                    downloadDB(&new_socket, com[2]);
                    writeLog(&current, buffer);
                }

                else
                {
                    char message[1024] = "Database not found!\n";
                    send(new_socket, message, strlen(message), 0);
                }
            }

            // Invalid command
            else
            {
                char message[1024] = "Invalid command!\n";
                send(new_socket, message, strlen(message), 0);
            }

            memset(buffer, 0, sizeof(buffer));
            memset(com, 0, sizeof(com));
        }
    }

    return 0;
}
