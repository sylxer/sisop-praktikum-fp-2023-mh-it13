#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <ctype.h>
#include <pthread.h>
#include <dirent.h>
#include <fcntl.h>

#define PORT 8080

int sock = 0, valread;
int status = 0;
pthread_t thread;

// Check if user is root
bool isRoot()
{
    return (getuid() == 0);
}

bool authenticate(int argc, char const *argv[])
{
    char buffer[1024] = {0};
    if (isRoot())
        send(sock, "root", sizeof("root"), 0);

    else if (argc != 6 || strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0)
    {
        send(sock, "error", strlen("error"), 0);
        printf("Authentication error!\n");
        printf("Syntax to start the client:\n");
        printf("./program -u [username] -p [password]\n");
        return false;
    }

    else
    {
        send(sock, argv[2], strlen(argv[2]), 0);
        valread = read(sock, buffer, 1024);
        send(sock, argv[4], strlen(argv[4]), 0);
    }

    memset(buffer, 0, sizeof(buffer));
    return true;
}

void *messageHandling(void *arg)
{
    char buffer[1024];

    memset(buffer, 0, sizeof(buffer));
    while (read(*(int *)arg, buffer, 1024) != 0)
    {
        if (strlen(buffer))
            printf("%s", buffer);

        memset(buffer, 0, sizeof(buffer));
    }
}

int main(int argc, char const *argv[])
{
    struct sockaddr_in address,
        serv_addr;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("failed to create socket\n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
        printf("Invalid address\n");
        return -1;
    }
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("Connection failed\n");
        return -1;
    }

    if (!authenticate(argc, argv))
        return 0;

    char buffer[1024];

    // Command for dump: BACKUP DATABASE [database_name]

    strcpy(buffer, "BACKUP DATABASE ");
    strcat(buffer, argv[5]);
    strcat(buffer, ";");

    pthread_t dataHandler;
    pthread_create(&dataHandler, NULL, &messageHandling, &sock);

    send(sock, buffer, strlen(buffer), 0);

    memset(buffer, 0, sizeof(buffer));

    // Join thread
    pthread_join(dataHandler, NULL);
    return 0;
}
